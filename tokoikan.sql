-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 26 Nov 2017 pada 08.57
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tokoikan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ikan`
--

CREATE TABLE `ikan` (
  `id_ikan` int(32) NOT NULL,
  `nama_ikan` varchar(200) NOT NULL,
  `harga` int(11) NOT NULL,
  `ketersedian` varchar(200) NOT NULL,
  `path_gambar` text NOT NULL,
  `tgl_create` date NOT NULL,
  `tgl_update` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `isi_keranjang`
--

CREATE TABLE `isi_keranjang` (
  `id_keranjang` int(32) NOT NULL,
  `id_ikan` int(32) NOT NULL,
  `jumlah` int(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `keranjang`
--

CREATE TABLE `keranjang` (
  `id_keranjang` int(32) NOT NULL,
  `id_user` int(32) NOT NULL,
  `status` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(32) NOT NULL,
  `id_keranjang` int(32) NOT NULL,
  `tgl_create` varchar(200) NOT NULL,
  `tgl_update` varchar(200) NOT NULL,
  `path_bukti_pembayaran` text NOT NULL,
  `status_transaksi` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(32) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `no_hp` varchar(12) NOT NULL,
  `alamat` text NOT NULL,
  `password` varchar(200) NOT NULL,
  `role` varchar(10) DEFAULT 'member',
  `path_foto` varchar(1000) DEFAULT 'default.png'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `nama`, `email`, `no_hp`, `alamat`, `password`, `role`, `path_foto`) VALUES
(35, 'Arlian', 'arlian@arlian.com', '123123', '123123', '4297f44b13955235245b2497399d7a93', 'member', NULL),
(36, 'Arlian', 'aasdasd@asdasd', '123123123', '123123123213', 'e10adc3949ba59abbe56e057f20f883e', 'member', NULL),
(37, 'asdasdasd', '123123123123@123', '123123123', '112123123', 'f5bb0c8de146c67b44babbf4e6584cc0', 'member', NULL),
(38, '123123', '123123123123@12123', '123123123', '123123123', '4297f44b13955235245b2497399d7a93', 'member', NULL),
(39, 'ADMIN LAMBE', 'admin@admin', '08563037200', 'Sanankulon Blitar', 'c849c1977fa1668c80cde5a516c8d6ae', 'member', '1511682272_Capture1.PNG'),
(40, 'helmi', 'helmi@helmi', '085670123123', 'Malang Jawa Timur', 'e10adc3949ba59abbe56e057f20f883e', 'member', NULL),
(41, 'yahya yahya', 'arwan@arwan', '085630111101', 'Malang Jawa Timur', 'e10adc3949ba59abbe56e057f20f883e', 'member', NULL),
(42, 'Fairus Zain', 'fair@fair.com', '088123817120', 'Kutub Utara, Sebelah Rumah Penjual Mie Ayam', 'e10adc3949ba59abbe56e057f20f883e', 'member', 'default.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ikan`
--
ALTER TABLE `ikan`
  ADD PRIMARY KEY (`id_ikan`);

--
-- Indexes for table `isi_keranjang`
--
ALTER TABLE `isi_keranjang`
  ADD KEY `id_keranjang` (`id_keranjang`),
  ADD KEY `id_ikan` (`id_ikan`);

--
-- Indexes for table `keranjang`
--
ALTER TABLE `keranjang`
  ADD PRIMARY KEY (`id_keranjang`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `id_keranjang` (`id_keranjang`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
