<?php $this->load->view('header'); ?>
<div id="pageContent">
	<div class="container offset-18">
		<h1 class="block-title large">Transaksi</h1>
		<?php foreach ($transaksi as $var): ?>
			<div class="offset-36">
				<h4>Transaksi #<?= $var->id_transaksi ?></h4>
				<div class="offset-30">
					<div class="responsive-table-order-history-02">
						<table class="table-order-history-02">
							<thead>
								<tr>
									<th colspan="3"></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="1">total</td></td>
									<td>Rp. <?= number_format($var->total,0,",","."); ?> <a href="#" data-toggle="modal" data-target="#ModalDetail<?= $var->id_keranjang ?>" class="link">
										( lihat detail )
									</a></td>
								</tr>
								<tr>
									<td colspan="1">Status Pembayaran </td>
									<td colspan=""><?= $var->status ?> </td>
								</tr>
								<tr>
									<td colspan="1">Tanggal Pembelian </td>
									<td colspan=""><?= date('d M Y ( h:i )', strtotime($var->tgl_create))  ?> </td>
								</tr>
								<tr>
									<td colspan="1">Alamat Pengiriman 
										<a href="#" data-toggle="modal" data-target="#ModalEdit<?= $var->id_transaksi ?>" class="link">
											( ubah )
										</a>
									</td>
									<td colspan=""><?= $var->alamat ?> </td>
								</tr>
								<tr>
									<td colspan="1">No.Hp Penerima <a href="#" data-toggle="modal" data-target="#ModalEdit<?= $var->id_transaksi ?>" class="link">
										( ubah )
									</a></td>
									<td colspan=""><?= $var->no_hp ?> </td>
								</tr>
								<tr>
									<td colspan="1">Bukti Pembayaran</td>
									<td colspan=""><a href="#" data-toggle="modal" data-target="#ModalBuktiTransaksi<?= $var->id_transaksi ?>" class="link">
										lihat
									</a></td>
								</tr>
								<tr>
									<td colspan="4">

										<div class="col-md-6"><strong>Upload/Ubah Bukti Pembayaran</strong>
											<form action="<?= site_url('controllerorderikan/uploadbuktipembayaran') ?>" method="post" enctype="multipart/form-data">
												<input style="margin-top: 10px" class="" type="file" name="bukti_pembayaran" required="" accept="image/*">
												<input class="" type="hidden" name="id_transaksi" value="<?= $var->id_transaksi ?>">
												<button style="margin-top: 10px" type="submit" class="btn">
													UPLOAD
												</button>
											</form>
										</div>
									</td>

								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="modal  fade"  id="ModalEdit<?= $var->id_transaksi ?>" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-md">
					<div class="modal-content ">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
						</div>
						<div class="modal-body">
							<!--  -->
							<div class="row">
								<div align="center" class="title"> <h4>UBAH DATA PENERIMA</h4></div>
								<div class="col-md-12">
									<div class="shopping-cart-box">
										<form action="<?= site_url('controllerorderikan/editdatatransaksi') ?>" method="post">		
											<div class="form-group">
												<label for="inputZip">NOMOR TELEPON</label>
												<input type="number" class="form-control" id="inputZip" required="" name="nohp" value="<?= $var->no_hp ?>">
											</div>
											<div class="form-group">
												<label for="inputZip">ALAMAT PENGIRIMIMAN</label>
												<input type="type" class="form-control" id="inputZip" required="" name="alamat" value="<?= $var->alamat ?>">
											</div>

											<div class="shopping-cart-box">
												<table class="table-total">
												</table>
												<input type="hidden" name="id_transaksi" value="<?= $var->id_transaksi ?>">
												<button type="submit" class="btn btn-full"><span class="icon icon-check_circle"></span>UBAH DATA</button>
											</div>
										</form>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal  fade"  id="ModalBuktiTransaksi<?= $var->id_transaksi ?>" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-md">
					<div class="modal-content ">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
						</div>
						<div class="modal-body">
							<!--  -->
							<div class="row">
								<div class="col-md-12">
									<div class="image-box">
										<img style="max-width: 100%" src="<?= base_url() ?>res/img/buktibayar/<?= $var->path_bukti_pembayaran ?>">
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>	
		<?php endforeach ?>
	</div>
</div>

<?php foreach ($total as $value): ?>
	<div class="modal  fade"  id="ModalDetail<?= $value->id_keranjang ?>" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content ">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
				</div>
				<div id="pageContent">
					<div class="container  offset-18">
						<h1 class="block-title large">Detail Pesanan</h1>
						<table class="shopping-cart-table" style="">
							<tbody>

								<?php foreach ($keranjang as $var): ?>
									<?php if ($value->id_keranjang==$var->id_keranjang): ?>
										<tr>
											<td>
												<div class="product-image">
													<a href="product.html">
														<img src="<?= base_url() ?>res/img/product/<?= $var->path_gambar ?>" >
													</a>
												</div>
											</td>
											<td>
												<h4 class="">
													<a href="product.html"><?= $var->nama_ikan ?></a>
												</h4>
												<ul class="list-parameters">
													<li class="visible-xs visible-sm">
														<div class="product-price unit-price">Rp. <?= number_format($var->harga,0,",","."); ?></div>
													</li>
													<li class="visible-xs visible-sm">
														<div class="product-price unit-price"><b><?= $var->jumlah ?></b> Kg</div>
													</li>
													<li class="visible-xs visible-sm">
														<div class="product-price subtotal">Rp. <?= number_format($var->total,0,",","."); ?></div>
													</li>
												</ul>
											</td>
											<td>
												<div class="product-price unit-price" align="right">
													Rp. <?= number_format($var->harga,0,",","."); ?> /Kg
												</div>
											</td>
											<td>
												<div class="product-price unit-price" align="right">
													<b><?= number_format($var->jumlah,0,",","."); ?></b> Kg
												</div>						
											</td>
											<td>
												<div class="product-price subtotal" align="right">
													Rp. <?= number_format($var->total,0,",","."); ?>
												</div>
											</td>
										</tr>		
									<?php endif ?>

								<?php endforeach ?>
								<tfoot>
									<tr>
										<td>
											<li class="visible-xs visible-sm">
												<div class="product-price unit-price">
													<b>Total :</b>
												</div>
											</li>
											<td>
												<li class="visible-xs visible-sm">
													<div class="product-price subtotal">Rp. <?= number_format($value->total,0,",","."); ?></div>
												</li>
											</td>
											<td></td>
											<td align="right">
												<div class="product-price unit-price">
													<b>Total :</b>
												</div>
											</td>
											<td colspan="">
												<div class="product-price subtotal" align="right">
													Rp. <?= number_format($value->total,0,",","."); ?>
												</div>
											</td>
											<td></td>
										</tr>
									</tfoot>
									<tr>
									</tfoot>
								</tbody>
							</table>
						</div>
					</div>

				</div>
			</div>
		</div>	
	<?php endforeach ?>


	<?php $this->load->view('footer'); ?>
