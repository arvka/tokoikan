<?php $this->load->view("component/header"); ?>
<!DOCTYPE html>
<html>
<head>

	<link href="<?php echo base_url(); ?>res/css/font-awesome.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>res/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>res/css/animate.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>res/css/owl.carousel.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>res/css/owl.theme.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>res/css/style.default.css" rel="stylesheet" id="theme-stylesheet">
	<link href="<?php echo base_url(); ?>res/css/custom.css" rel="stylesheet">
	<script src="<?php echo base_url(); ?>res/js/respond.min.js"></script>
	<link rel="shortcut icon" href="favicon.png">
	<title></title>
</head>
<body>

	<?php
	if($this->session->flashdata('updateprofil')) { ?>
	<div class="alert alert-success" align="center">
		<strong>Anda berhasil Update Profil!</strong>
	</div>
	<?php } ?>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-default">
					<div class="panel-heading">  <h4 >User Profile</h4></div>
					<div class="panel-body">
						<div class="box box-info">
							<div class="box-body">
								<?php
								foreach($users as $use){ 
									?>
									<div class="col-sm-6 col-sm-offset-3" align="">
										<div  align="center"> <img alt="User Pic" 
											src="<?php echo base_url(); ?>upload/fotouser/<?= $use->path_foto ?>" id="profile-image1" class="img-circle img-responsive" align="center"> 

											<input id="profile-image-upload" class="hidden" type="file">

											<!--Upload Image Js And Css-->
										</div>
										<br>

										<!-- /input-group -->
									</div>

									<div class="clearfix"></div>
									<hr style="margin:5px 0 5px 0;">
									<div class="center-block" align="center">


										<h4><?php echo $use->nama ?></h4>
										<span><?php echo $use->email ?></span><br>
										<span><?php echo $use->no_hp ?></span><br>
										<span><?php echo $use->alamat ?></span>	<br>

										<?php } ?>
									</div>

									<!-- /.box-body -->
								</div>
								<!-- /.box -->


							</div>
							<div align="right">
								<a href="<?php echo site_url('usercontroller/viewEditProfil') ?>" class="btn btn-info" ><i class="fa fa-edit"></i><span class="hidden-sm">Edit Profil</span></a>
							</div>

						</div> 
					</div>
				</div>  
				<script>
					$(function() {
						$('#profile-image1').on('click', function() {
							$('#profile-image-upload').click();
						});
					});       
				</script> 
			</div>
		</div>
	</body>
	</html>
	<?php $this->load->view("component/footer"); ?>