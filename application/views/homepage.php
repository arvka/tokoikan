<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="robots" content="all,follow">
        <meta name="googlebot" content="index,follow,snippet,archive">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="">

        <title>
            Tokoikan.id : situs penjualan ikan apa saja
        </title>

        <meta name="keywords" content="">

        <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

        <!-- styles -->
        <link rel="stylesheet" href="res/css/bootstrap.min.css">
        <link href="res/css/font-awesome.css" rel="stylesheet">
        <link href="res/css/animate.min.css" rel="stylesheet">
        <link href="res/css/owl.carousel.css" rel="stylesheet">
        <link href="res/css/owl.theme.css" rel="stylesheet">

        <!-- theme stylesheet -->
        <link href="res/css/style.default.css" rel="stylesheet" id="theme-stylesheet">

        <!-- your stylesheet with modifications -->
        <link href="res/css/custom.css" rel="stylesheet">

        <script src="res/js/respond.min.js"></script>

        <link rel="shortcut icon" href="res/favicon.png">



    </head>

    <body>

        <?php $this->load->view("component/header"); ?>

        <div id="all">

            <div id="content">

                <div class="container">
                    <div class="col-md-12">
                        <div id="main-slider">
                            <div class="item">
                                <img src="res/img/main-slider1.jpg" alt="" class="img-responsive">
                            </div>
                            <div class="item">
                                <img class="img-responsive" src="res/img/main-slider2.jpg" alt="">
                            </div>
                            <div class="item">
                                <img class="img-responsive" src="res/img/main-slider3.jpg" alt="">
                            </div>
                            <div class="item">
                                <img class="img-responsive" src="res/img/main-slider4.jpg" alt="">
                            </div>
                        </div>
                        <!-- /#main-slider -->
                    </div>
                </div>

                <!-- *** ADVANTAGES HOMEPAGE ***
     _________________________________________________________ -->
                <div id="advantages">

                    <div class="container">
                        <div class="same-height-row">
                            <div class="col-sm-4">
                                <div class="box same-height clickable">
                                    <div class="icon">
                                        <span class="fa fa-heart"></span>
                                    </div>

                                    <h3><a href="#">We love our customers</a></h3>
                                    <p>We are known to provide best possible service ever</p>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="box same-height clickable">
                                    <div class="icon"><i class="fa fa-tags"></i>
                                    </div>

                                    <h3><a href="#">Best prices</a></h3>
                                    <p>You can check that the height of the boxes adjust when longer text like this one is used in one of them.</p>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="box same-height clickable">
                                    <div class="icon"><i class="fa fa-thumbs-up"></i>
                                    </div>

                                    <h3><a href="#">100% satisfaction guaranteed</a></h3>
                                    <p>Free returns on everything for 3 months.</p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->

                    </div>
                    <!-- /.container -->

                </div>
                <!-- /#advantages -->

                <!-- *** ADVANTAGES END *** -->

                <!-- *** HOT PRODUCT SLIDESHOW ***
     _________________________________________________________ -->
                <div id="hot">
                    <div class="text-center" data-animate="fadeInUp">
                        <div class="box">
                            <div class="container">
                                <div class="col-md-12">
                                    <h2>PRODUK IKAN TERLARIS</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center" data-animate="fadeInUp">
                            <div class="container">
                                <div class="product-slider">
                                    <?php foreach ($dIkan as $row) { ?>
                                    <div class="item">
                                        <div class="product">
                                            <a href="<?php echo site_url('ikanController/ViewEditPostingIkan')."?ikan=$row->nama_ikan"?>">
                                                <img src="res/img/product/<?php echo $row->path_gambar ?>" alt="" class="img-responsive">
                                            </a>
                                            <div class="text">
                                                <h3><a href="<?php echo site_url('ikanController/ViewEditPostingIkan')."?ikan=$row->nama_ikan"?>"><?php echo $row->nama_ikan ?></a></h3>
                                                <a class="price">Rp. <?php echo $row->harga ?>/Kg</a>
                                            </div>
                                            <!-- /.text -->
                                        </div>
                                        <!-- /.product -->
                                    </div>
                                    <?php } ?>
                                </div>
                                <!-- /.product-slider -->
                            </div>
                            <!-- /.container -->
                        </div>
                </div>
                <!-- /#hot -->

                <!-- *** HOT END *** -->

                <!-- *** GET INSPIRED ***
     _________________________________________________________ -->

                <!-- *** GET INSPIRED END *** -->

                <!-- *** BLOG HOMEPAGE ***
     _________________________________________________________ -->


                <!-- /#content -->
                <?php $this->load->view('component/footer'); ?>
            </div>
            <!-- /#all -->




            <!-- *** SCRIPTS TO INCLUDE ***
         _________________________________________________________ -->
            <script src="<?php echo base_url(); ?>/res/js/jquery-1.11.0.min.js"></script>
            <script src="<?php echo base_url(); ?>/res/js/bootstrap.min.js"></script>
            <script src="<?php echo base_url(); ?>/res/js/jquery.cookie.js"></script>
            <script src="<?php echo base_url(); ?>/res/js/waypoints.min.js"></script>
            <script src="<?php echo base_url(); ?>/res/js/modernizr.js"></script>
            <script src="<?php echo base_url(); ?>/res/js/bootstrap-hover-dropdown.js"></script>
            <script src="<?php echo base_url(); ?>/res/js/owl.carousel.min.js"></script>
            <script src="<?php echo base_url(); ?>/res/js/front.js"></script>


    </body>

</html>