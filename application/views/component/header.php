<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
    </head>
    <body>
        <!-- *** TOPBAR ***
            _________________________________________________________ -->
        <div id="top">
            <div class="container">
                <div class="col-md-6 offer" data-animate="fadeInDown">

                </div>
                <div class="col-md-6" data-animate="fadeInDown">
                    <ul class="menu">

                        <?php if ($this->session->userdata('login')) { ?>
                            <li><a>Halo <?php echo "<strong>".$this->session->userdata('nama')."</strong>"; ?> !</a> </li>
                            <li><a href="<?php echo site_url('orderikancontroller/viewStatusTransaksi') ?>">Status Transaksi</a></li> 
                            <li><a href="<?php echo site_url('usercontroller/viewProfil') ?>">My Profile</a></li> 
                            <li><a href="<?php echo site_url('usercontroller/logout') ?>">logout</a></li>
                        <?php } else {
                            ?>
                            <li><a href=" <?php echo site_url('usercontroller/viewLogin') ?>" >Login</a>
                            </li>
                            <li><a href="<?php echo site_url('usercontroller/viewRegister') ?>">Register</a>
                            </li>        
                        <?php } ?>
                    </ul>
                </div>
            </div>

        </div>

        <!-- *** TOP BAR END *** -->

        <!-- *** NAVBAR ***
         _________________________________________________________ -->

        <div class="navbar navbar-default yamm" role="navigation" id="navbar">
            <div class="container">
                <div class="navbar-header">

                    <a class="navbar-brand home" href="<?php echo base_url(); ?>" data-animate-hover="bounce">
                        <img src="<?php echo base_url(); ?>/res/img/logo.png" alt="Tokoikan logo" class="hidden-xs">
                        <img src="<?php echo base_url(); ?>/res/img/logo-small.png" alt="Tokoikan logo" class="visible-xs"><span class="sr-only">Obaju - go to homepage</span>
                    </a>
                    <div class="navbar-buttons">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
                            <span class="sr-only">Toggle navigation</span>
                            <i class="fa fa-align-justify"></i>
                        </button>
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#search">
                            <!--                        <span class="sr-only">Toggle search</span>-->
                            <img src='<?php echo base_url(); ?>/res/img/icons/glyphicons-28-search.png'>
                        </button>
                        <a class="btn btn-default navbar-toggle" href="basket.html">
                            <i class="glyphicon glyphicon-shopping-cart"></i>  <span class="hidden-xs">3 items in cart</span>
                        </a>
                    </div>
                </div>
                <!--/.navbar-header -->

                <div class="navbar-collapse collapse" id="navigation">

                    <ul class="nav navbar-nav navbar-left">
                        <li class="active"><a href="<?php echo site_url('home') ?>">Home</a>
                        </li>
                    </ul>
                </div>
                <!--/.nav-collapse -->

                <div class="navbar-buttons">

                    <div class="navbar-collapse collapse right" id="basket-overview">
                        <a href="<?php echo site_url('orderikancontroller/viewkeranjangbelanja') ?>" class="btn btn-primary navbar-btn"><i class="fa fa-shopping-cart"></i><span class="hidden-sm">0 items in cart</span></a>
                    </div>
                    <!--/.nav-collapse -->

                    <div class="navbar-collapse collapse right" id="search-not-mobile">
                        <button type="button" class="btn navbar-btn btn-primary" data-toggle="collapse" data-target="#search">
                            <span class="sr-only">Toggle search</span>
                            <!--<span><img src="<?php // echo base_url();  ?>/res/img/icons/glyphicons-28-search.png"></span>-->
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>

                <div class="collapse clearfix" id="search">


                <form action="<?php echo site_url('ikanController/CariIkan')?>" method="GET" class="navbar-form" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" name="keyword" placeholder="Search">
                        <span class="input-group-btn">


                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>

                            </span>
                        </div>
                    </form>

                </div>
                <!--/.nav-collapse -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#navbar -->

        <!-- *** NAVBAR END *** -->

    </body>
</html>
