<br><br>
<hr>
<footer style="margin-top: 5px">
	<div class="footer-content-col" style="margin-bottom : 10px; padding: 10px">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-3">
					<div class="mobile-collapse">
						<!-- <h4 class="mobile-collapse_title visible-xs">Gratis Ongkir</h4> -->
						<div class="mobile-collapse_content">
							<a href="<?= base_url() ?>res/#" class="services-block">
								<span class="icon icon-airplanemode_active"></span>
								<div class="title">Gratis Ongkir</div>
								<p>untuk malang dan sekitarnya</p>
							</a>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-3">
					<div class="mobile-collapse">
						<!-- <h4 class="mobile-collapse_title visible-xs">Belanja Aman</h4> -->
						<div class="mobile-collapse_content">
							<a href="<?= base_url() ?>res/#" class="services-block">
								<span class="icon icon-security"></span>
								<div class="title">Belanja Aman</div>
								<p>Menjual Ikan kualitas terbaik</p>
							</a>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-3">
					<div class="mobile-collapse">
						<!-- <h4 class="mobile-collapse_title visible-xs">PENGIRIMAN CEPAT</h4> -->
						<div class="mobile-collapse_content">
							<a href="<?= base_url() ?>res/#" class="services-block">
								<span class="icon icon-assignment_return"></span>
								<div class="title">PENGIRIMAN CEPAT</div>
								<p>Kurang dari 3 Hari</p>
							</a>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-3">
					<div class="mobile-collapse">
						<!-- <h4 class="mobile-collapse_title visible-xs">SUPPORT</h4> -->
						<div class="mobile-collapse_content">
							<a href="<?= base_url() ?>res/#" class="services-block">
								<span class="icon icon-headset_mic"></span>
								<div class="title">Support</div>
								<p>Suport Penuh</p>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="copyright" style="margin : 0px" >
		<div class="container visible-xs">
			<!-- <div class="social-icon-round">
				<ul>
					<li><a class="icon fa fa-facebook" href="<?= base_url() ?>res/http://www.facebook.com/"></a></li>
					<li><a class="icon fa fa-twitter" href="<?= base_url() ?>res/http://www.twitter.com/"></a></li>
					<li><a class="icon fa fa-google-plus" href="<?= base_url() ?>res/http://www.google.com/"></a></li>
					<li><a class="icon fa fa-instagram" href="<?= base_url() ?>res/https://instagram.com/"></a></li>
				</ul>
			</div> -->
		</div>
		<div class="container">
			<!-- <div class="pull-right">
				<div class="payment-list">
					<ul>
						<li><a class="icon-01" href="<?= base_url() ?>"></a></li>
						<li><a class="icon-02" href="<?= base_url() ?>"></a></li>
						<li><a class="icon-03" href="<?= base_url() ?>"></a></li>
						<li><a class="icon-04" href="<?= base_url() ?>"></a></li>
						<li><a class="icon-05" href="<?= base_url() ?>"></a></li>
						<li><a class="icon-06" href="<?= base_url() ?>"></a></li>
						<li><a class="icon-07" href="<?= base_url() ?>"></a></li>
						<li><a class="icon-08" href="<?= base_url() ?>"></a></li>
					</ul>
				</div>
			</div> -->
			<div class="" align="center">
				<div class="box-copyright">
					<a href="<?= base_url() ?>res/index.html">Tokoikan</a> &copy; 2017. <span>RPL E | FILKOM UB.</span>
				</div>
			</div>
		</div>
	</div>
	<a href="<?= base_url() ?>#" class="back-to-top">
		<span class="icon icon-keyboard_arrow_up"></span>
		<span class="text">KEMBALI KE ATAS</span>
	</a>




	<script src="<?= base_url() ?>res/external/jquery/jquery-2.1.4.min.js"></script>
	<script src="<?= base_url() ?>res/external/bootstrap/bootstrap.min.js"></script>
	<script src="<?= base_url() ?>res/external/countdown/jquery.plugin.min.js"></script>
	<script src="<?= base_url() ?>res/external/countdown/jquery.countdown.min.js"></script>
	<script src="<?= base_url() ?>res/external/slick/slick.min.js"></script>
	<script src="<?= base_url() ?>res/external/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script src="<?= base_url() ?>res/external/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	<script src="<?= base_url() ?>res/external/panelmenu/panelmenu.js"></script>
	<script src="<?= base_url() ?>res/js/quick-view.js"></script>
	<script src="<?= base_url() ?>res/js/main.js"></script>


	<?php
	if($this->session->flashdata('logingagal')) { ?>
	<script type="text/javascript">
		$(window).load(function(){
			$('#modalLoginForm').modal('show');
		});
	</script>
	<?php } ?>

	<?php
	if($this->session->flashdata('logindulu')) { ?>
	<script type="text/javascript">
		$(window).load(function(){
			$('#modalLoginForm').modal('show');
		});
	</script>
	<?php } ?>

</footer>