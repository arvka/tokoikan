<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('header'); ?>

<div class="breadcrumb">
	<div class="container">
		<ul>
			<li><a href="index.html">Home</a></li>
			<li>Register</li>
		</ul>
	</div>
</div>
<!-- Content -->
<div id="pageContent">
	<div class="container offset-14">

		<h1 class="block-title large">Register</h1>
		<div class="row">
			

			<div class="col-sm-8 col-sm-push-2 col-lg-6 col-lg-push-3">
				<div class="login-form-box">
					<?php 
					if (isset($message)) { ?>
					<div class="alert alert-success">                           
						<strong>Success!</strong> Anda berhasil membuat akun.
					</div>
					<?php 
					unset($message); } ?>                        
					<?php 
					if (isset($warning)) { ?>
					<div class="alert alert-danger">
						<strong>Email sudah digunakan!</strong> silahkan guanakan email lain
					</div>
					<?php 
					unset($warning); } ?>
					<h2 class="text-uppercase">Informasi Pribadi</h2>
					<form action="<?php echo site_url('controlleruser/register')?>" method="post">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">
									<span class="icon icon-person_outline"></span>
								</span>
								<input type="text" id="LoginFormName1" class="form-control" placeholder="Nama" name="nama">
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">
									<span class="icon icon-email"></span>
								</span>
								<input type="email" id="LoginEmail" class="form-control" placeholder="E-mail" name="email">
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">
									<span class="icon icon-lock_outline"></span>
								</span>
								<input type="password" id="LoginFormPass1" class="form-control" placeholder="Password" name="password">
							</div>
						</div>

						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">
									<span class="icon icon-phone"></span>
								</span>
								<input type="number" id="LoginEmail" class="form-control" placeholder="Nomor HP" name="nohp">
							</div>
						</div>


						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">
									<span class="icon icon-home"></span>
								</span>
								<input type="text" id="LoginEmail" class="form-control" placeholder="Alamat" name="alamat">
							</div>
						</div>

						<div class="row">

							<div class="col-md-12">
								<div class="button-block">
									<button type="submit" class="btn">REGISTER</button>
								</div>
							</div>
							<div class="col-md-12">
								<div class="additional-links-01">Sudah punya akun ? <a href="#" data-toggle="modal" data-target="#modalLoginForm"><span class="icon icon-lock_outline">Login</a></div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php $this->load->view('footer'); ?>