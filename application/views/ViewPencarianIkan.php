<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="robots" content="all,follow">
        <meta name="googlebot" content="index,follow,snippet,archive">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Obaju e-commerce template">
        <meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
        <meta name="keywords" content="">

        <title>
            Obaju : e-commerce template
        </title>

        <meta name="keywords" content="">

        <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

        <!-- styles -->
        <link href="<?php echo base_url(); ?>res/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>res/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>res/css/animate.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>res/css/owl.carousel.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>res/css/owl.theme.css" rel="stylesheet">

        <!-- theme stylesheet -->
        <link href="<?php echo base_url(); ?>res/css/style.default.css" rel="stylesheet" id="theme-stylesheet">

        <!-- your stylesheet with modifications -->
        <link href="<?php echo base_url(); ?>res/css/custom.css" rel="stylesheet">

        <script src="<?php echo base_url(); ?>res/js/respond.min.js"></script>

        <link rel="shortcut icon" href="favicon.png">



    </head>

    <body>
        <!-- *** TOPBAR ***
     _________________________________________________________ -->
        <?php $this->load->view("component/header"); ?>

        <!-- *** NAVBAR END *** -->

        <div id="all">

            <div id="content">
                <div class="container">

                    <div class="col-md-12">
                        <ul class="breadcrumb">
                            <li><a href="#">Home</a>
                            </li>
                            <li>Search</li>
                            <li>Hasil cari</li>
                        </ul>
                    </div>


                    <div id="hot">

                        <div class="box">
                            <div class="container">
                                <div class="col-md-12">
                                    <h2>Search</h2>
                                </div>
                            </div>
                        </div>


                        <div class="text-center" data-animate="fadeInUp">
                            <div class="container">
                                <div class="product-slider">
                                    <?php foreach ($dIkan as $row) { ?>
                                        <div class="item">
                                            <div class="product">
                                                <a href="<?php echo site_url('ikancontroller/ViewEditPostingIkan?ikan='.$row->nama_ikan) ?>">
                                                    <img src="<?php echo base_url(); ?>res/img/product/<?php echo $row->path_gambar ?>" alt="" class="img-responsive">
                                                </a>
                                                <div class="text">
                                                    <h3><a href="<?php echo site_url('ikancontroller/ViewEditPostingIkan') ?>"><?php echo $row->nama_ikan ?></a></h3>
                                                    <p class="price">Rp. <?php echo $row->harga ?>/Kg</p>
                                                </div>
                                                <!-- /.text -->
                                            </div>
                                            <!-- /.product -->
                                        </div>
                                    <?php } ?>

                                </div>
                                <!-- /.product-slider -->
                            </div>
                            <!-- /.container -->



                        </div>
                    </div>
                    <!-- /.col-md-9 -->
                </div>
                <!-- /.container -->
            </div>
            <!-- /#content -->


            <!-- *** FOOTER ***
     _________________________________________________________ -->
            <?php $this->load->view('component/footer'); ?>
            <!-- *** COPYRIGHT END *** -->



        </div>
        <!-- /#all -->




        <!-- *** SCRIPTS TO INCLUDE ***
     _________________________________________________________ -->
        <script src="<?php echo base_url(); ?>/res/js/jquery-1.11.0.min.js"></script>
        <script src="<?php echo base_url(); ?>/res/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>/res/js/jquery.cookie.js"></script>
        <script src="<?php echo base_url(); ?>/res/js/waypoints.min.js"></script>
        <script src="<?php echo base_url(); ?>/res/js/modernizr.js"></script>
        <script src="<?php echo base_url(); ?>/res/js/bootstrap-hover-dropdown.js"></script>
        <script src="<?php echo base_url(); ?>/res/js/owl.carousel.min.js"></script>
        <script src="<?php echo base_url(); ?>/res/js/front.js"></script>




        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>

        <script>
            function initialize() {
                var mapOptions = {
                    zoom: 15,
                    center: new google.maps.LatLng(49.1678136, 16.5671893),
                    mapTypeId: google.maps.MapTypeId.ROAD,
                    scrollwheel: false
                }
                var map = new google.maps.Map(document.getElementById('map'),
                        mapOptions);

                var myLatLng = new google.maps.LatLng(49.1681989, 16.5650808);
                var marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map
                });
            }

            google.maps.event.addDomListener(window, 'load', initialize);
        </script>


    </body>

</html>
