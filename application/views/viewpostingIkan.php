	<?php $this->load->view('header'); ?>
	<br><br>				
	<div id="pageContent">
		<div class="post blog-single-post">
			<div class="container offset-17">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<?php
						if($this->session->flashdata('postikan')) { ?>
						<div class="alert alert-success">
							<strong>Berhasil!</strong> Anda berhasil membuat post ikan..
						</div>
						<?php } ?>
						<h1 class="block-title large">Posting Ikan</h1>
						<form action="<?php echo site_url('controllerikan/postingikan')?>" method="post" enctype="multipart/form-data">
							<div class="form-group">
								<label for="inputName" class="col-sm-3 control-label">Nama</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="inputName" placeholder="Nama Ikan" name="namaikan" required="">
								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail" class="col-sm-3 control-label">Harga/KG</label>
								<div class="col-sm-9">
									<input type="number" class="form-control" id="inputEmail" placeholder="Harga Ikan/KG" name="harga" required="">
								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail" class="col-sm-3 control-label">Ketersediaan</label>
								<div class="col-sm-9">
									<ul class="list-simple list-simple-inline">
										<li>
											<label class="radio">
												<input id="radio11" type="radio" name="ketersediaan" checked value="tersedia">
												<span class="outer"><span class="inner"></span></span>Tersedia</label>
											</li>
											<li>
												<label class="radio">
													<input id="radio12" type="radio" name="ketersediaan"" value="habis">
													<span class="outer"><span class="inner"></span></span>Stock Habis</label>
												</li>
											</ul>
										</div>
									</div>
									<div class="form-group">
										<label for="inputEmail" class="col-sm-3 control-label">Upload Gambar Ikan</label>
										<div class="col-sm-9">
											<input type="file" class="form-control" id="inputFile" placeholder="Gambar Ikan" accept="image/*" name="foto" required="">
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-offset-3 col-sm-9">
											<button type="submit" class="btn">POST IKAN</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php $this->load->view('footer'); ?>	