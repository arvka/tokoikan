<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Obaju e-commerce template">
    <meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
    <meta name="keywords" content="">

    <title>
        Obaju : e-commerce template
    </title>

    <meta name="keywords" content="">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="<?php echo base_url(); ?>res/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>res/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>res/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>res/css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>res/css/owl.theme.css" rel="stylesheet">

    <!-- theme stylesheet -->
    <link href="<?php echo base_url(); ?>res/css/style.default.css" rel="stylesheet" id="theme-stylesheet">

    <!-- your stylesheet with modifications -->
    <link href="<?php echo base_url(); ?>res/css/custom.css" rel="stylesheet">

    <script src="<?php echo base_url(); ?>res/js/respond.min.js"></script>

    <link rel="shortcut icon" href="favicon.png">



</head>

<body>
    <!-- *** TOPBAR ***
 _________________________________________________________ -->
   <?php $this->load->view("component/header"); ?>

    <!-- *** NAVBAR END *** -->

    <div id="all">

        <div id="content">
            <div class="container">

                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <h1>Daftar Member</h1>
                    </ul>
                </div>

                <div class="col-md-12" id="basket">

                    <div class="box">
                        <form method="post" action="checkout1.html">

                            
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>                                            
                                            <th>Id</th>
                                            <th>Nama</th>
                                            <th>Email</th>
                                            <th>Alamat</th>                                    
                                            <th colspan="3">Keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><a href="#">1111</a>
                                            </td>
                                            <td>Siapa aja punya</td>
                                                                                     
                                            <td>
                                                siapaajapunya@punya.com
                                            </td>
                                            <td>jalan mana aja dong
                                            </td> 
                                            <td>
                                                <button type="button" class="btn btn-danger btn-sm">Edit</button>
                                                <button type="button" class="btn btn-danger btn-sm">Hapus</button>
                                               
                                            </td>
                                        </tr>
                                        
                                    </tbody>
                                </table>

                            </div>
                            <!-- /.table-responsive -->

                            

                        </form>

                    


                </div>
                <!-- /.col-md-9 -->

                

                   
                </div>
                <!-- /.col-md-3 -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->

        <!-- *** FOOTER ***
 _________________________________________________________ -->
        <?php $this->load->view('component/footer');?>
        <!-- *** COPYRIGHT END *** -->



    </div>
    <!-- /#all -->


    

    <!-- *** SCRIPTS TO INCLUDE ***
 _________________________________________________________ -->
    <script src="<?php echo base_url(); ?>/res/js/jquery-1.11.0.min.js"></script>
    <script src="<?php echo base_url(); ?>/res/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>/res/js/jquery.cookie.js"></script>
    <script src="<?php echo base_url(); ?>/res/js/waypoints.min.js"></script>
    <script src="<?php echo base_url(); ?>/res/js/modernizr.js"></script>
    <script src="<?php echo base_url(); ?>/res/js/bootstrap-hover-dropdown.js"></script>
    <script src="<?php echo base_url(); ?>/res/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url(); ?>/res/js/front.js"></script>



</body>

</html>