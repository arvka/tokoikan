<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Obaju e-commerce template">
    <meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
    <meta name="keywords" content="">

    <title>
        Obaju : e-commerce template
    </title>

    <meta name="keywords" content="">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="<?php echo base_url(); ?>res/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>res/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>res/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>res/css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>res/css/owl.theme.css" rel="stylesheet">

    <!-- theme stylesheet -->
    <link href="<?php echo base_url(); ?>res/css/style.default.css" rel="stylesheet" id="theme-stylesheet">

    <!-- your stylesheet with modifications -->
    <link href="<?php echo base_url(); ?>res/css/custom.css" rel="stylesheet">

    <script src="<?php echo base_url(); ?>res/js/respond.min.js"></script>

    <link rel="shortcut icon" href="favicon.png">



</head>

<body>
    <?php $this->load->view("component/header"); ?>
    <div id="all">

        <div id="content">
            <div class="container">

                <div class="col-md-12">

                    <ul class="breadcrumb">
                        <li><a href="#">Home</a>
                        </li>
                        <li>Sign in</li>
                    </ul>

               

                </div>
                <br>
                <div class='col-md-12' align="center" >
                     <?php
                        if($this->session->flashdata('logout')) { ?>
                        <div class="alert alert-success">
                           <strong>Anda berhasil logout!</strong> terimakasih telah berkunjung di website kami
                       </div>
                       <?php } ?>
                </div>              
                <div class="col-md-4 col-md-offset-4">
                    <div class="box">
                        <?php
                        if($this->session->flashdata('logingagal')) { ?>
                        <div class="alert alert-danger">
                           <strong>Username atau Password salah!</strong> silahkan login kembali..
                       </div>
                       <?php } ?>

                        <h1>Login</h1>
                        <p class="text-muted">Login sekarang untuk dapat mulai menggunakan sebagai member.</p>
                        <hr>
                        
                       <form action="<?php echo site_url('usercontroller/login')?>" method="post">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" required="" value="<?php echo set_value('email'); ?>">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password" required="" value="<?php echo set_value('password'); ?>">
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-sign-in"></i> Log in</button>
                        </div>
                    </form>
                </div>
            </div>


        </div>
        <!-- /.container -->
    </div>
    <!-- /#content -->
    <?php $this->load->view('component/footer'); ?>
</div>
<!-- /#all -->




    <!-- *** SCRIPTS TO INCLUDE ***
       _________________________________________________________ -->
       <script src="<?php echo base_url(); ?>res/js/jquery-1.11.0.min.js"></script>
       <script src="<?php echo base_url(); ?>res/js/bootstrap.min.js"></script>
       <script src="<?php echo base_url(); ?>res/js/jquery.cookie.js"></script>
       <script src="<?php echo base_url(); ?>res/js/waypoints.min.js"></script>
       <script src="<?php echo base_url(); ?>res/js/modernizr.js"></script>
       <script src="<?php echo base_url(); ?>res/js/bootstrap-hover-dropdown.js"></script>
       <script src="<?php echo base_url(); ?>res/js/owl.carousel.min.js"></script>
       <script src="<?php echo base_url(); ?>res/js/front.js"></script>



   </body>

   </html>
