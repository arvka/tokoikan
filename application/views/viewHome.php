
<?php $this->load->view('header'); ?>
<!-- Content -->
<br>
<div id="pageContent">
	<div class="container-fluid offset-0">
		<div class="row">
			<div class="slider-revolution revolution-default">
				<div class="tp-banner-container">
					<div class="tp-banner revolution">
						<ul>
							<li data-thumb="<?= base_url() ?>res/images/slides/04/slide-1.jpg" data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off"  data-title="Slide">
								<img src="<?= base_url() ?>res/images/slides/04/slide-1.jpg"  alt="slide1"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" >
								<div class="tp-caption tp-resizeme  lfb stb text-center"
								data-x="center"
								data-y="center"
								data-hoffset="0"
								data-voffset="-20"
								data-speed="600"
								data-start="900"
								data-easing="Power4.easeOut"
								data-endeasing="Power4.easeIn"
								data-responsive_offset="on"
								style="z-index: 2;">
								<div class="tp-caption1-wd-1 color-white">Our Experience Gives us the Ability to</div>
								<div class="tp-caption1-wd-2 color-white">Create Stunning<br>Webstore</div>
								<div class="tp-caption1-wd-3"><a href="<?= base_url() ?>res/listing-left-column.html" class="btn btn-lg" data-text="Shop now!">SHOP NOW!</a></div>
							</div>
						</li>
						<li data-thumb="<?= base_url() ?>res/images/slides/04/slide-2.jpeg" data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off"  data-title="Slide">
							<img src="<?= base_url() ?>res/images/slides/04/slide-2.jpeg"  alt="slide1"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" >
							<div class="tp-caption tp-resizeme  lfr str text-right"
							data-x="right"
							data-y="center"
							data-voffset="-20"
							data-hoffset="-351"
							data-speed="600"
							data-start="900"
							data-easing="Power4.easeOut"
							data-endeasing="Power4.easeIn"
							data-responsive_offset="on"
							style="z-index: 2;">
							<div class="tp-caption1-wd-1 color-base">We'll Help to Manage</div>
							<div class="tp-caption1-wd-2">Your Online<br>Business</div>
							<div class="tp-caption1-wd-3"><a href="<?= base_url() ?>res/listing-left-column.html" class="btn btn-lg btn-red" data-text="Shop now!">SHOP NOW!</a></div>
						</div>
					</li>
					<li data-thumb="<?= base_url() ?>res/images/slides/04/slide-3.jpg" data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off"  data-title="Slide">
						<img src="<?= base_url() ?>res/images/slides/04/slide-3.jpg"  alt="slide1"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" >
						<div class="tp-caption tp-resizeme  lfr str text-right"
						data-x="right"
						data-y="center"
						data-voffset="-20"
						data-hoffset="-351"
						data-speed="600"
						data-start="900"
						data-easing="Power4.easeOut"
						data-endeasing="Power4.easeIn"
						data-responsive_offset="on"
						style="z-index: 2;">
						<div class="tp-caption1-wd-1 color-base">We'll Help to Manage</div>
						<div class="tp-caption1-wd-2">Your Online<br>Business</div>
						<div class="tp-caption1-wd-3"><a href="<?= base_url() ?>res/listing-left-column.html" class="btn btn-lg btn-red" data-text="Shop now!">SHOP NOW!</a></div>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>
</div>
</div>


<div class="container hidden-mobile">
	<hr>
</div>
<div class="container">
	<h1 class="block-title">DAFTAR IKAN</h1>
	<div class="row product-listing carousel-products-mobile products-mobile-arrow">
		<?php foreach ($data as $var): ?>
			<div class="col-xs-6 col-sm-4 col-md-3">
				<div class="product">
					<div class="product_inside">
						<div class="image-box">
							
							<?php if ($var->ketersedian=='tersedia'): ?>
								<div class="label-new" style="background-color: #5fd179">Tersedia</div>
							<?php else: ?>
								<div class="label-sale">Stock Habis</div>
							<?php endif ?>


							<img src="<?= base_url() ?>res/img/product/<?= $var->path_gambar ?>" style="height: 150px" alt="">

							<!-- <a href="#" data-toggle="modal" data-target="#ModalquickView" class="quick-view" id="pilihikan">
								<span>
									<span class="icon icon-visibility"></span>TAMBAH KE KERANJANG
								</span>
							</a> -->
						</div>
						<h2 class="title">
							<H4><?= $var->nama_ikan ?></H4>
						</h2>
						<div class="price view">
							Rp. <?= number_format($var->harga,0,",","."); ?> <label>/Kg</label>
						</div>
						<!-- <div class="description">
							Silver, metallic-blue and metallic-lavender silk-blend jacquard, graphic pattern, pleated ruffle along collar, long sleeves with button-fastening cuffs, buckle-fastening silver skinny belt, large pleated rosettes at hips. Dry clean. Zip and hook fastening at back. 100% silk. Specialist clean
						</div> -->
						<div class="product_inside_hover">
							<div class="product_inside_info">
								<?php if ($var->ketersedian=='tersedia'): ?>
								<a class="btn btn-product_addtocart" data-toggle="modal" data-target="#ModalquickView<?= $var->id_ikan?>" class="quick-view" id="pilihikan">
									<span class="icon icon-shopping_basket"></span>TAMBAH KE KERANJANG
								</a>
							<?php else: ?>
								<a class="btn btn-product_addtocart" data-toggle="modal" data-target="#modalHabis" class="quick-view" id="pilihikan">
									<span class="icon icon-shopping_basket"></span>TAMBAH KE KERANJANG
								</a>
							<?php endif ?>
							
								
								<!-- <a href="#"  class="quick-view btn" data-toggle="modal" data-target="#ModalquickView" id="lihatikan">
									<span>
										<span class="icon icon-visibility"></span>TAMBAH KE KERANJANG
									</span>
								</a> -->
								<!-- <ul class="product_inside_info_link">
									<li>
										<a href="#" data-toggle="modal" data-target="#ModalquickView" class="quick-view" id="lihatikan">
											<span class="icon icon-visibility"></span>
										</a>
									</li>
								</ul> -->
							</div>
						</div>
					</div>
				</div>
			</div>	
		<?php endforeach ?>
		
	</div>
</div>
<div class="container hidden-mobile">
</div>

</div>

<!-- Modal (quickViewModal) -->
<?php foreach ($data as $var): ?>
	<div class="modal  fade"  id="ModalquickView<?= $var->id_ikan ?>" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content ">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
				</div>
				<form action="<?= site_url('controllerorderikan/tambahkeranjang/') ?><?= $var->id_ikan ?>" method="post">
					<div class="modal-body">
						<!--modal-quick-view-->
						<div class="modal-quick-view">
							<div class="row">
								<div class="col-sm-5 col-lg-6">
									<div class="product-main-image">
										<img src="<?= base_url() ?>res/img/product/<?= $var->path_gambar ?>"  alt="">
									</div>
								</div>
								<div class="col-sm-7 col-lg-6">
									<div class="product-info">
										<div class="add-info">

											<div class="availability pull-left">
												<span class="font-weight-medium color-defaulttext2">Ketersediaan:</span> <span class="color-red"><?= $var->ketersedian ?></span>
											</div>
										</div>
										<h1 class="title"><?= $var->nama_ikan ?></h1>
										<div class="price">
											Rp. <?= number_format($var->harga,2,",","."); ?><span> /Kg</sapan>
											</div>
									<!-- <div class="description hidden-xs">
										<div class="text">
											Silver, metallic-blue and metallic-lavender silk-blend jacquard, graphic pattern, pleated ruffle along collar, long sleeves with button-fastening cuffs, buckle-fastening silver skinny belt, large pleated rosettes at hips.
										</div>
									</div> -->
									<div class="wrapper">
										
										<div class="pull-left"><label class="qty-label"> <H5>Pembelian maks 50 Kg</H5></label></div>
									</div>
									
									<div class="wrapper">
										<div class="pull-left"><label class="qty-label"> <H5>Jumlah (Kg)</H5></label></div>
										<div class="pull-left">
											<input style="text-align: center;" class="form-control" type="number" name="jumlah" value="1" min="1" max="50"> 
										</div>
									</div>

									<div class="wrapper">
										
										<div class="pull-left">
											<button type="submit" class="btn btn-addtocart"><span class="icon icon-shopping_basket"></span>TAMBAH KE KERANJANG</button>
										</div>
									</div>									
								</div>
							</div>
						</div>
					</div>
					<!--/modal-quick-view-->
				</div>
			</form>
		</div>
	</div>
</div>
<?php endforeach ?>

<!-- / Modal (quickViewModal) -->
<!-- Modal (newsletter) -->
<!-- / Modal (newsletter) -->
<!-- modalLoginForm-->
<!-- /modalLoginForm-->
<!-- modalAddToCart -->
<div class="modal  fade"  id="modalHabis" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content ">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
			</div>
			<div class="modal-body">
				<!--  -->
				<div class="modal-add-cart">
					<span class="icon color-base icon-close_circle"></span>
					<p>
						Mohon Maaf, Stock Ikan Habis !
					</p>
				</div>
				<!-- / -->
			</div>
		</div>
	</div>
</div>
<!-- /modalAddToCart -->
<!-- modalAddToCartProduct -->

<!-- /modalAddToCartProduct -->
<!-- modalCompare -->

<!-- /modalCompare -->
<!-- modalWishlist -->


<?php $this->load->view('footer'); ?>
<!-- /modalWishlist -->

</body>
</html> 