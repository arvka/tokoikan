<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>admin toko.ikan</title>


        <!-- MetisMenu CSS -->
<!--        <link href="<?php // echo base_url(); ?>res/css/metisMenu.css" rel="stylesheet">-->

        <!-- Custom CSS -->
        <link href="<?php echo base_url(); ?>res/css/sb-admin-2.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <!--<link href="<?php // echo base_url(); ?>res/css/morris.css" rel="stylesheet">-->

        <link href="<?php echo base_url(); ?>res/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>res/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>res/css/animate.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>res/css/owl.carousel.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>res/css/owl.theme.css" rel="stylesheet">


    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <?php $this->load->view('component/AdminHeader'); ?>
            <div id="page-wrapper">
                <h1 class="title">DAFTAR USER</h1>
                <table class="table table-hover">
                        <tr>
                            <th>
                                id
                            </th>
                            <th>
                                Nama Ikan
                            </th>
                            <th>
                                Foto
                            </th>
                            <th>
                                Harga
                            </th>
                            <th>
                                Ketersediaan
                            </th>
                            <th>
                                Tgl Buat
                            </th>
                            <th>
                                Tgl Update
                            </th>
                            <th colspan="2">
                                Action
                            </th>
                        </tr>
                        <?php foreach ($data as $row) { ?>
                        <tr>
                            
                            <td>
                                <?php echo $row->id_ikan; ?>
                            </td>
                            <td>
                                <?php echo $row->nama_ikan; ?>
                            </td>
                            <td>
                                <img src=<?php echo $row->path_gambar; ?> style="width:50dp;height:50dp;">
                            </td>
                            <td>
                                <?php echo $row->harga; ?>
                            </td>
                            <td>
                                <?php echo $row->ketersedian; ?>
                            </td>
                            <td>
                                <?php echo $row->tgl_create; ?>
                            </td>
                            <td>
                                <?php echo $row->tgl_update; ?>
                            </td>
                            <td>
                                <button class="btn btn-xs btn-info" name="Edit" value="Edit">EDIT</button>
                            </td>
                            <td>
                                <button class="btn btn-xs btn-danger" name="Delet" value="delete">DELETE</button>
                            </td>
                        </tr>
                        <?php } ?>
                </table>
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->

        <!-- Metis Menu Plugin JavaScript -->
<!--        <script src="<?php // echo base_url(); ?>res/js/metisMenu.min.js"></script>-->
        <!--<script src="<?php // echo base_url('res/js/bootstrap.min.js'); ?>"></script>-->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <!--<script src="<?php // echo base_url('res/js/jquery.min.js'); ?>"></script>-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="<?php echo base_url('res/js/sb-admin-2.js'); ?>"></script>
    </body>
</html>
