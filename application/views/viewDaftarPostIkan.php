<?php $this->load->view('header'); ?>
<br><br>
<div id="pageContent">
	<div class="container offset-18">
		<h1 class="block-title large">Daftar Ikan</h1>
		<?php if ($this->session->flashdata('suksesedit')): ?>
			<div class="alert alert-success" align="center">                           
				<strong>Sukses! </strong> Berhasil update data user
			</div>
		<?php endif ?>

		<?php if ($this->session->flashdata('sukseshapus')): ?>
			<div class="alert alert-success" align="center">                           
				<strong>Sukses! </strong> Berhasil hapus data user
			</div>
		<?php endif ?>

		<?php if ($this->session->flashdata('updateikan')): ?>
			<div class="alert alert-success" align="center">                           
				<strong>Sukses! </strong> Berhasil update data ikan
			</div>
		<?php endif ?>
		<div class="pull-right"><a href="<?= site_url('controllerikan/viewPostingIKan') ?>" class="btn" style="max-height: 40px"	><span class="fa fa-plus"></span>Tambah Post Ikan</a></div>
		<br>
		<div class="offset-36">
			<table class="table-order-history">
				<thead>
					<tr>
						<th>Id Ikan</th>
						<th>Nama_Ikan</th>
						<th>Harga</th>
						<th>Ketersediaan</th>
						<th>Foto</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($ikan  as $var): ?>
						<tr>
							<td valign="middle">
								<!-- <div class="th-title visible-xs">Order</div> -->
								<a href="#">#<?= $var->id_ikan?></a>
							</td>
							<td valign="middle" style="">
								<!-- <div class="th-title visible-xs">Date</div> -->
								<?= $var->nama_ikan?>
							</td >
							<td valign="middle">
								<!-- <div class="th-title visible-xs">Payment Status</div> -->
								Rp. <?= number_format($var->harga,0,",","."); ?> /Kg
							</td>
							<td valign="middle">
								<!-- <div class="th-title visible-xs">Fulfillment Status</div> -->
								<?= $var->ketersedian ?>
							</td>
							<td valign="middle">
								<!-- <div class="th-title visible-xs">Fulfillment Status</div> -->
								<a href="" data-toggle="modal" data-target="#ModalViewGambar<?= $var->id_ikan ?>" class="link" style="max-height: 30px">lihat foto</a>								
							</td>

							<td valign="middle">
								<!-- <div class="th-title visible-xs">Total</div> -->
								<a href="<?= site_url('controllerikan/viewEditPostIkan/') ?><?= $var->id_ikan ?>" class="btn" style="max-height: 30px">Edit</a> 
								<a href="" data-toggle="modal" data-target="#ModalActionHapus<?= $var->id_ikan ?>" class="btn btn-red" style="max-height: 30px">Hapus</a>
							</td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>	
	</div>
</div>

<?php foreach ($ikan as $var): ?>
	<div class="modal  fade"  id="ModalActionHapus<?= $var->id_ikan ?>" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-md-small">
			<div class="modal-content ">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
				</div>
				<div class="modal-body">
					<!--  -->
					<div class="row" align="center">

						<h4>Apakah anda yakin menghapus ikan tersebut</h4>
						<a href="<?= site_url('controllerikan/hapuspostikan/') ?><?= $var->id_ikan ?>" class="btn" style="max-height: 30px">Iya</a>
						<a data-dismiss="modal" aria-hidden="true" class="btn btn-red" style="max-height: 30px">Tidak</a>

					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal  fade"  id="ModalViewGambar<?= $var->id_ikan ?>" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-md">
			<div class="modal-content ">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
				</div>
				<div class="modal-body">
					<!--  -->
					<div class="row" align="center">
						<img style="width:100% " src="<?= base_url() ?>res/img/product/<?= $var->path_gambar ?>">
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endforeach ?>

<?php $this->load->view('footer'); ?>
