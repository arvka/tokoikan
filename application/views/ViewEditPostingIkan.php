<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="robots" content="all,follow">
        <meta name="googlebot" content="index,follow,snippet,archive">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Obaju e-commerce template">
        <meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
        <meta name="keywords" content="">

        <title>
            Obaju : e-commerce template
        </title>

        <meta name="keywords" content="">

        <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

        <!-- styles -->
        <link href="<?php echo base_url(); ?>res/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>res/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>res/css/animate.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>res/css/owl.carousel.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>res/css/owl.theme.css" rel="stylesheet">

        <!-- theme stylesheet -->
        <link href="<?php echo base_url(); ?>res/css/style.default.css" rel="stylesheet" id="theme-stylesheet">

        <!-- your stylesheet with modifications -->
        <link href="<?php echo base_url(); ?>res/css/custom.css" rel="stylesheet">

        <script src="<?php echo base_url(); ?>res/js/respond.min.js"></script>

        <link rel="shortcut icon" href="favicon.png">



    </head>

    <body>
        <?php $this->load->view("component/header"); ?>

        <div id="all">

            <div id="content">
                <div class="container">

                    <div class="col-md-12">
                        <ul class="breadcrumb">
                            <li><a href="#">Home</a>
                            </li>
                            <li>Edit Posting Ikan</li>
                        </ul>
                    </div>

                    <form>
                        <?php foreach ($dIkan as $ikan) { ?>
                        <div class="form-group row">
                            <label for="inputIkan" class="col-sm-2 col-form-label">Nama Ikan</label>
                            <div class="col-sm-10">
                                <input type="Text" class="form-control" id="inputIkan" placeholder="Nama Ikan" value="<?php echo $ikan->nama_ikan ?>">
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="inputharga" class="col-sm-2 col-form-label">Harga</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" id="inputharga" placeholder="Harga" value="<?php echo $ikan->harga ?>">
                            </div>
                        </div>
                        <fieldset class="form-group">
                            <div class="row">
                                <legend class="col-form-legend col-sm-2">Ketersediaan</legend>
                                <div class="col-sm-10" value="<?php echo $ikan->ketersedian ?>">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" checked>
                                            Tersedia
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="option2">
                                            Habis
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <div class="form-group">
                            <label for="exampleFormControlFile1">Upload Gambar</label>
                            <input type="file" class="form-control-file" id="exampleFormControlFile1" accept="image/*">
                        </div>
                        <input type="hidden" id="idIkan" value="<?php echo $ikan->id_ikan ?>"/>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary">Posting</button>
                            </div>
                        </div>
                        <?php } ?>
                    </form>

                </div>
                <!-- /.col-md-9 -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->


        <!-- *** FOOTER ***
 _________________________________________________________ -->
        <?php $this->load->view('component/footer'); ?>
        <!-- /#footer -->

        <!-- *** FOOTER END *** -->




        <!-- *** COPYRIGHT ***
 _________________________________________________________ -->

        <!-- /#all -->




        <!-- *** SCRIPTS TO INCLUDE ***
     _________________________________________________________ -->
        <script src="js/jquery-1.11.0.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.cookie.js"></script>
        <script src="js/waypoints.min.js"></script>
        <script src="js/modernizr.js"></script>
        <script src="js/bootstrap-hover-dropdown.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/front.js"></script>




        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>

        <script>
            function initialize() {
                var mapOptions = {
                    zoom: 15,
                    center: new google.maps.LatLng(49.1678136, 16.5671893),
                    mapTypeId: google.maps.MapTypeId.ROAD,
                    scrollwheel: false
                }
                var map = new google.maps.Map(document.getElementById('map'),
                        mapOptions);

                var myLatLng = new google.maps.LatLng(49.1681989, 16.5650808);
                var marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map
                });
            }

            google.maps.event.addDomListener(window, 'load', initialize);
        </script>


    </body>

</html>
