<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Obaju e-commerce template">
    <meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
    <meta name="keywords" content="">

    <title>
        STATUS TRANSAKSI - TOKOIKAN
    </title>

    <meta name="keywords" content="">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="<?php echo base_url(); ?>res/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>res/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>res/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>res/css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>res/css/owl.theme.css" rel="stylesheet">

    <!-- theme stylesheet -->
    <link href="<?php echo base_url(); ?>res/css/style.default.css" rel="stylesheet" id="theme-stylesheet">

    <!-- your stylesheet with modifications -->
    <link href="<?php echo base_url(); ?>res/css/custom.css" rel="stylesheet">

    <script src="<?php echo base_url(); ?>res/js/respond.min.js"></script>

    <link rel="shortcut icon" href="favicon.png">



</head>

<body>
    <?php $this->load->view("component/header"); ?>
    <div id="all">

        <div id="content">
            <div class="container">

                <div class="col-md-12">

                    <ul class="breadcrumb">
                        <li><a href="#">Home</a>
                        </li>
                        <li>Edit Profil</li>
                    </ul>

                </div>


                <div class="col-md-12">
                    <div class="box">
                        <h1>DAFTAR TRANSAKSI ANDA</h1>
                        <hr>
                        <?php 
                            if ($this->session->flashdata('sukses') != null) { 
                                ?>
                                <div class="alert alert-success">                           
                                    <strong>Success!</strong> <?php echo $this->session->flashdata('sukses');?>
                                </div>
                        <?php 
                            } else if($this->session->flashdata('fail') != null){ ?>
                                <div class="alert alert-danger">                           
                                    <strong>Gagal!</strong> <?php echo $this->session->flashdata('fail');?>
                                </div>
                            <?php } ?>
                        <table class="table table-hover">
                            <tr>
                                <th>
                                    Nomor Transaksi
                                </th>
                                <th>
                                    Tanggal Transaksi
                                </th>
                                <th>
                                    Total
                                </th>
                                <th>
                                    Status Verifikasi
                                </th>
                                <th>
                                    Upload Bukti Pembayaran
                                </th>
                            </tr>
                            <?php foreach ($dStatusTransaksi as $row) { ?>
                            <tr>
                                <td>
                                    <?php echo $row['id']; ?>
                                </td>
                                <td>
                                    <?php echo $row['tanggal']; ?>
                                </td>
                                <td>
                                    Rp. <?php echo $row['total']; ?>
                                </td>
                                <td>
                                    <?php 
                                        if($row['status'] == 'belum'){
                                            echo "Belum Diverifikasi";
                                        }else{
                                            echo "Sudah Diverifikasi";
                                        }
                                    ?>
                                </td>
                                <?php echo form_open("orderikancontroller/unggahbuktipembayaran", array('enctype'=>'multipart/form-data')); ?>
                                <td>
                                    <?php
                                        if($row['bukti'] != null || $row['bukti'] != ''){?>
                                            Bukti sudah diupload
                                    <?php
                                        }else{ ?>
                                            <input type="hidden" value="<?php echo $row['id']; ?>" name="idTransaksi">
                                            <input type="file" name="gambarBukti" accept="image/*">
                                                <input type="submit" name="btn-upload" value="Submit" class="btn btn-info">
                                    <?php
                                        }
                                    ?>
                                    
                                </td>
                                <?php echo form_close(); ?>
                            </tr>
                            <?php } ?>
                        </table>   
                    </div>
                </div>


            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->

        <?php $this->load->view('component/footer');?>
    </div>
    <!-- /#all -->


    

    <!-- *** SCRIPTS TO INCLUDE ***
     _________________________________________________________ -->
     <script src="<?php echo base_url(); ?>res/js/jquery-1.11.0.min.js"></script>
     <script src="<?php echo base_url(); ?>res/js/bootstrap.min.js"></script>
     <script src="<?php echo base_url(); ?>res/js/jquery.cookie.js"></script>
     <script src="<?php echo base_url(); ?>res/js/waypoints.min.js"></script>
     <script src="<?php echo base_url(); ?>res/js/modernizr.js"></script>
     <script src="<?php echo base_url(); ?>res/js/bootstrap-hover-dropdown.js"></script>
     <script src="<?php echo base_url(); ?>res/js/owl.carousel.min.js"></script>
     <script src="<?php echo base_url(); ?>res/js/front.js"></script>



 </body>

 </html>
