<?php $this->load->view('header'); ?>
<br><br>
<div id="pageContent">
	<div class="container offset-18">
		<h1 class="block-title large">Daftar User</h1>
		<?php if ($this->session->flashdata('suksesedit')): ?>
			<div class="alert alert-success" align="center">                           
				<strong>Sukses! </strong> Berhasil update data user
			</div>
		<?php endif ?>

		<?php if ($this->session->flashdata('sukseshapus')): ?>
			<div class="alert alert-success" align="center">                           
				<strong>Sukses! </strong> Berhasil hapus data user
			</div>
		<?php endif ?>

		<?php if ($this->session->flashdata('batalverifikasi')): ?>
			<div class="alert alert-success" align="center">                           
				<strong>Sukses! </strong> Berhasil Membatalkan Verifikasi Pembelian
			</div>
		<?php endif ?>
		<div class="pull-right"><a href="<?= site_url('controlleruser/viewRegister') ?>" class="btn" style="max-height: 40px"	><span class="fa fa-plus"></span>Tambah User</a></div>
		<br>
		<div class="offset-36">
			<table class="table-order-history">
				<thead>
					<tr>
						<th>Id User</th>
						<th>Nama</th>
						<th>Alamat</th>
						<th>email</th>
						<th>NO HP</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($user as $var): ?>
						<tr>
							<td valign="middle">
								<!-- <div class="th-title visible-xs">Order</div> -->
								<a href="#">#<?= $var->id_user?></a>
							</td>
							<td valign="middle" style="">
								<!-- <div class="th-title visible-xs">Date</div> -->
								<?= $var->nama?>
							</td >
							<td valign="middle"><?= $var->alamat ?></td>
							<td valign="middle">
								<!-- <div class="th-title visible-xs">Payment Status</div> -->
								<?= $var->email ?>
							</td>
							<td valign="middle">
								<!-- <div class="th-title visible-xs">Fulfillment Status</div> -->
								<?= $var->no_hp ?>
							</td>
							

							<td valign="middle">
								<!-- <div class="th-title visible-xs">Total</div> -->
								<a href="<?= site_url('controlleruser/viewEditProfilAdmin/') ?><?= $var->id_user ?>" class="btn" style="max-height: 30px">Edit</a> 
								<a href="" data-toggle="modal" data-target="#ModalActionHapus<?= $var->id_user ?>" class="btn btn-red" style="max-height: 30px">Hapus</a>
							</td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>	
	</div>
</div>

<?php foreach ($user as $var): ?>
	<div class="modal  fade"  id="ModalActionHapus<?= $var->id_user ?>" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-md-small">
						<div class="modal-content ">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
							</div>
							<div class="modal-body">
								<!--  -->
								<div class="row" align="center">
									
										<h4>Apakah anda yakin menghapus user tersebut</h4>
										<a href="<?= site_url('controlleruser/hapususer/') ?><?= $var->id_user ?>" class="btn" style="max-height: 30px">Iya</a>
										<a data-dismiss="modal" aria-hidden="true" class="btn btn-red" style="max-height: 30px">Tidak</a>
									
								</div>
							</div>
						</div>
					</div>
				</div>
<?php endforeach ?>

<?php $this->load->view('footer'); ?>
