<?php $this->load->view('header'); ?>
<div class="breadcrumb">
	<div class="container">
		<ul>
			<li><a href="index.html">Home</a></li>
			<li>Keranjang Belanja</li>
		</ul>
	</div>
</div>
<div id="pageContent">
	<?php if ($this->session->flashdata('hapus')): ?>
		<div class="alert alert-success" align="center">                           
			<strong>Sukses! </strong> ikan dihapus dari keranjang
		</div>
	<?php endif ?>
	<?php if ($this->session->userdata('keranjang')==0): ?>
		<h1 class="block-title large">Keranjang Kosong</h1>
		<br><br><br><br><br><br>
	<?php else: ?>
		<div class="container  offset-18">
			<h1 class="block-title large">Keranjang Belanja</h1>
			<!-- Shopping cart table -->
			<?php if ($this->session->flashdata('tambahkeranjang')): ?>
				<div class="alert alert-success" align="center">                           
					<strong>Sukses! </strong> Berhasil menambah ikan ke keranjang anda</div>
				<?php endif ?>
				
				<table class="shopping-cart-table" style="">

					<tbody>

						<?php foreach ($keranjang as $var): ?>
							<tr>
								<td>
									<div class="product-image">
										<a href="product.html">
											<img src="<?= base_url() ?>res/img/product/<?= $var->path_gambar ?>" >
										</a>
									</div>
								</td>
								<td>
									<h4 class="">
										<a href="product.html"><?= $var->nama_ikan ?></a>
									</h4>
									<ul class="list-parameters">
										<li class="visible-xs visible-sm">
											<div class="product-price unit-price">Rp. <?= number_format($var->harga,0,",","."); ?></div>
										</li>
										<li class="visible-xs visible-sm">
											<div class="product-price unit-price"><b><?= $var->jumlah ?></b> Kg</div>
										</li>
										<li class="visible-xs visible-sm">
											<div class="product-price subtotal">Rp. <?= number_format($var->total,0,",","."); ?></div>
										</li>
									</ul>
								</td>
								<td>
									<div class="product-price unit-price" align="right">
										Rp. <?= number_format($var->harga,0,",","."); ?> /Kg
									</div>
								</td>
								<td>
									<div class="product-price unit-price" align="right">
										<b><?= number_format($var->jumlah,0,",","."); ?></b> Kg
									</div>						
								</td>
								<td>
									<div class="product-price subtotal" align="right">
										Rp. <?= number_format($var->total,0,",","."); ?>
									</div>
								</td>
								<td>
									<a class="product-delete icon icon-delete" href="<?= site_url('controllerorderikan/hapusisikeranjang') ?>/<?= $var->id_isi_keranjang ?>""></a>
								</td>
							</tr>	
						<?php endforeach ?>
						<?php foreach ($totalHarga as $var): ?>
							<tfoot>
								<tr>
									<td>
										<li class="visible-xs visible-sm">
											<div class="product-price unit-price">
												<b>Total :</b>
											</div>
										</li>
										<td>
											<li class="visible-xs visible-sm">
												<div class="product-price subtotal">Rp. <?= number_format($var->total,0,",","."); ?></div>
											</li>
										</td>
										<td></td>
										<td align="right">
											<div class="product-price unit-price">
												<b>Total :</b>
											</div>
										</td>
										<td colspan="">
											<div class="product-price subtotal" align="right">
												Rp. <?= number_format($var->total,0,",","."); ?>
											</div>
										</td>
										<td></td>
									</tr>
								</tfoot>
								<tr>
								</tfoot>
							<?php endforeach ?>

						</tbody>
					</table>

					<!-- /Shopping cart table -->
					<div class="shopping-cart-btns">
						<div class="pull-right">
							<a class="btn btn-full" href="#" data-toggle="modal" data-target="#modalBeliIkan""><span class="icon icon-check_circle"></span> Beli Ikan</a>
					<!-- <a class="btn-link" href="#"><span class="icon icon-cached color-base"></span>UPDATE CART</a>
					<div class="clearfix visible-xs visible-sm"></div>
					<a class="btn-link" href="#"><span class="icon icon-delete"></span>CLEAR SHOPPING CART</a> -->
				</div>
				<div class="pull-left">
					<a class="btn-link" href="<?= base_url() ?>"><span class="icon icon-keyboard_arrow_left"></span>Lanjut Belanja </a>
				</div>
			</div>
		</div>
	</div>
	
<?php endif ?>

<div class="modal  fade"  id="modalBeliIkan" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md-small">
		<div class="modal-content ">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
			</div>
			<div class="modal-body">
				<!--  -->
				<div class="row">
					<div align="center" class="title"> <h2>PEMBELIAN</h2></div>
					<div class="col-md-12">
						<div class="shopping-cart-box">
							<h4>DATA PENERIMA</h4>
							<form action="<?= site_url('controllerorderikan/pembelian') ?>" method="post">		
								<div class="form-group">
									<label for="inputZip">NOMOR TELEPON</label>
									<input type="number" class="form-control" id="inputZip" required="" name="nohp">
								</div>
								<div class="form-group">
									<label for="inputZip">ALAMAT PENGIRIMIMAN</label>
									<input type="type" class="form-control" id="inputZip" required="" name="alamat">
								</div>

								<div class="shopping-cart-box">
									<table class="table-total">
										<tfoot>
											<tr>
												<th>TOTAL:</th>

												<td><?php foreach ($totalHarga as $value): ?>

													Rp. <?= number_format($value->total,0,",","."); ?>
													<input type="hidden" name="id_keranjang" value="<?= $value->id_keranjang ?>">
												<?php endforeach ?></td>
											</tr>
										</tfoot>
									</table>
									<button type="submit" class="btn btn-full"><span class="icon icon-check_circle"></span>PROSES PEMBELIAN</button>
								</div>
							</form>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>
<?php $this->load->view('footer'); ?>