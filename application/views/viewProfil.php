<?php $this->load->view('header'); ?>
<div class="breadcrumb">
	<div class="container">
		<ul>
			<li><a href="index.html">Home</a></li>
			<li>Account</li>
		</ul>
	</div>
</div>
<!-- Content -->
<div id="pageContent">
	<div class="container offset-18">
		<h1 class="block-title large">Profil</h1>
		<div class="offset-36">
			<?php
			if($this->session->flashdata('updateprofil')) { ?>
			<h4><div class="alert alert-success" align="center">
				<strong>Anda berhasil Update Profil!</strong>
			</div></h5>
			
			<?php } ?>
			<h4>Detail Akun</h4>
			<?php foreach ($users as $var): ?>
				<div class="row">
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
						<div class="responsive-table">
							<table class="table table-params">
								<tbody>
									<tr>
										<td>Nama:</td>
										<td><?= $var->nama ?></td>
									</tr>
									<tr>
										<td>E-mail:</td>
										<td><?= $var->email ?></td>
									</tr>
									<tr>
										<td>Alamat:</td>
										<td><?= $var->alamat ?></td>
									</tr>
									<tr>
										<td>Phone:</td>
										<td><?= $var->no_hp ?></td>
									</tr>
								</tbody>
							</table>
						</div>
						<a href="<?= site_url('controlleruser/vieweditprofil') ?>" class="btn"><i class="icon icon-edit"></i>Edit Profil</a>
					</div>
				</div>
			</div>
		<?php endforeach ?>

	</div>
</div>
<hr class="hr-offset-7">
<?php $this->load->view('footer'); ?>