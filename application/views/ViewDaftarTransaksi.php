<?php $this->load->view('header'); ?>
<br><br>
<div id="pageContent">
	<div class="container offset-18">
		<h1 class="block-title large">Daftar Transaksi</h1>
		<?php if ($this->session->flashdata('verifikasi')): ?>
			<div class="alert alert-success" align="center">                           
				<strong>Sukses! </strong> Berhasil Verifikasi Pembayaran
			</div>
		<?php endif ?>

		<?php if ($this->session->flashdata('batalverifikasi')): ?>
			<div class="alert alert-success" align="center">                           
				<strong>Sukses! </strong> Berhasil Membatalkan Verifikasi Pembelian
			</div>
		<?php endif ?>
		<div class="offset-36">
			<hr class="hr-offset-7">
			<table class="table-order-history">
				<thead>
					<tr>
						<th>Id Transaksi</th>
						<th>Atas nama</th>
						<th>tgl Beli</th>
						<th>Total</th>
						<th>Status Pembayaran</th>
						<th>Verifikasi</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($transaksi as $var): ?>
						<tr>
							<td valign="middle">
								<!-- <div class="th-title visible-xs">Order</div> -->
								<a href="#">#<?= $var->id_transaksi?></a>
							</td>
							<td valign="middle">
								<!-- <div class="th-title visible-xs">Date</div> -->
								<?= $var->nama?><a href="#" data-toggle="modal" data-target="#Modaluser<?= $var->id_transaksi ?>" class="link">
									( lihat detail)
								</a>
							</td >
							<td valign="middle"><?= date('d M Y ', strtotime($var->tgl_create))  ?></td>
							<td valign="middle">
								<!-- <div class="th-title visible-xs">Payment Status</div> -->
								Rp. <?= number_format($var->total,0,",","."); ?><a href="#" data-toggle="modal" data-target="#ModalDetail<?= $var->id_keranjang ?>" class="link">
									( lihat detail )
									</a>
								</td>
								<td valign="middle">
									<!-- <div class="th-title visible-xs">Fulfillment Status</div> -->
									<?= $var->status ?><a href="" data-toggle="modal" data-target="#ModalBuktiTransaksi<?= $var->id_transaksi ?>" class="link">
										( lihat Bukti Pembayaran )
									</a>
								</td>
								<td valign="middle">
									<!-- <div class="th-title visible-xs">Total</div> -->
									<?php if ($var->status=='pending'): ?>
										<a href="" data-toggle="modal" data-target="#ModalVerifikasi<?= $var->id_transaksi ?>" class="btn" style="max-height: 30px">Verifikasi</a>	
									<?php else: ?>
										<a data-toggle="modal" data-target="#ModalVerifikasi<?= $var->id_transaksi ?>" href="" class="btn btn-red" style="max-height: 30px">Batal Verifikasi</a>
									<?php endif ?>
									
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>

			<?php foreach ($transaksi as $var): ?>
				<div class="modal  fade"  id="ModalEdit<?= $var->id_transaksi ?>" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-md">
						<div class="modal-content ">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
							</div>
							<div class="modal-body">
								<!--  -->
								<div class="row">
									<div align="center" class="title"> <h4>UBAH DATA PENERIMA</h4></div>
									<div class="col-md-12">
										<div class="shopping-cart-box">
											<form action="<?= site_url('controllerorderikan/editdatatransaksi') ?>" method="post">		
												<div class="form-group">
													<label for="inputZip">NOMOR TELEPON</label>
													<input type="number" class="form-control" id="inputZip" required="" name="nohp" value="<?= $var->no_hp ?>">
												</div>
												<div class="form-group">
													<label for="inputZip">ALAMAT PENGIRIMIMAN</label>
													<input type="type" class="form-control" id="inputZip" required="" name="alamat" value="<?= $var->alamat ?>">
												</div>

												<div class="shopping-cart-box">
													<table class="table-total">
													</table>
													<input type="hidden" name="id_transaksi" value="<?= $var->id_transaksi ?>">
													<button type="submit" class="btn btn-full"><span class="icon icon-check_circle"></span>UBAH DATA</button>
												</div>
											</form>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal  fade"  id="ModalBuktiTransaksi<?= $var->id_transaksi ?>" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-md">
						<div class="modal-content ">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
							</div>
							<div class="modal-body">
								<!--  -->
								<div class="row">
									<div class="col-md-12">
										<div class="image-box">
											<img style="max-width: 100%" src="<?= base_url() ?>res/img/buktibayar/<?= $var->path_bukti_pembayaran ?>">
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>


				<div class="modal  fade"  id="ModalVerifikasi<?= $var->id_transaksi ?>" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-md-small">
						<div class="modal-content ">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
							</div>
							<div class="modal-body">
								<!--  -->
								<div class="row" align="center">
									<?php if ($var->status=='pending'): ?>
										<h4>Apakah anda yakin melakukan verifikasi transaksi</h4>
										<a href="<?= site_url('controllerorderikan/verifikasipembayaran/') ?><?= $var->id_transaksi ?>/Accepted" class="btn" style="max-height: 30px">Iya</a>
										<a data-dismiss="modal" aria-hidden="true" class="btn btn-red" style="max-height: 30px">Tidak</a>
									<?php else: ?>
										<h4>Apakah yakin anda melakukan batal verifikasi transaksi</h4>
										<a href="<?= site_url('controllerorderikan/verifikasipembayaran/') ?><?= $var->id_transaksi ?>/pending" class="btn" style="max-height: 30px">Iya</a>
										<a data-dismiss="modal" aria-hidden="true" class="btn btn-red" style="max-height: 30px">Tidak</a>
									<?php endif ?>	
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal  fade"  id="Modaluser<?= $var->id_transaksi ?>" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-md">
						<div class="modal-content ">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
							</div>
							<div class="modal-body" style="margin-bottom: ">
								<!--  -->
								<div class="row">
									<div class="col-md-12">
										<div class="responsive-table-order-history-02">
											<table class="table-order-history-02">
												<tbody>
													<tr>
														<td colspan="1">Status Pembayaran </td>
														<td colspan=""><?= $var->status ?> </td>
													</tr>
													<tr>
														<td colspan="1">Tanggal Pembelian </td>
														<td colspan=""><?= date('d M Y ( h:i )', strtotime($var->tgl_create))  ?> </td>
													</tr>
													<tr>
														<td colspan="1">Alamat Pengiriman 

														</td>
														<td colspan=""><?= $var->alamat ?> </td>
													</tr>
													<tr>
														<td colspan="1">No.Hp Penerima </td>
														<td colspan=""><?= $var->no_hp ?> </td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>	
			<?php endforeach ?>
			
			


		</div>

	</div>

	<?php foreach ($total as $value): ?>
		<div class="modal  fade"  id="ModalDetail<?= $value->id_keranjang ?>" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content ">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
					</div>
					<div id="pageContent">
						<div class="container  offset-18">
							<h1 class="block-title large">Detail Pesanan</h1>
							<table class="shopping-cart-table" style="">
								<tbody>

									<?php foreach ($keranjang as $var): ?>
										<?php if ($value->id_keranjang==$var->id_keranjang): ?>
											<tr>
												<td>
													<div class="product-image">
														<a href="product.html">
															<img src="<?= base_url() ?>res/img/product/<?= $var->path_gambar ?>" >
														</a>
													</div>
												</td>
												<td>
													<h4 class="">
														<a href="product.html"><?= $var->nama_ikan ?></a>
													</h4>
													<ul class="list-parameters">
														<li class="visible-xs visible-sm">
															<div class="product-price unit-price">Rp. <?= number_format($var->harga,0,",","."); ?></div>
														</li>
														<li class="visible-xs visible-sm">
															<div class="product-price unit-price"><b><?= $var->jumlah ?></b> Kg</div>
														</li>
														<li class="visible-xs visible-sm">
															<div class="product-price subtotal">Rp. <?= number_format($var->total,0,",","."); ?></div>
														</li>
													</ul>
												</td>
												<td>
													<div class="product-price unit-price" align="right">
														Rp. <?= number_format($var->harga,0,",","."); ?> /Kg
													</div>
												</td>
												<td>
													<div class="product-price unit-price" align="right">
														<b><?= number_format($var->jumlah,0,",","."); ?></b> Kg
													</div>						
												</td>
												<td>
													<div class="product-price subtotal" align="right">
														Rp. <?= number_format($var->total,0,",","."); ?>
													</div>
												</td>
											</tr>		
										<?php endif ?>

									<?php endforeach ?>
									<tfoot>
										<tr>
											<td>
												<li class="visible-xs visible-sm">
													<div class="product-price unit-price">
														<b>Total :</b>
													</div>
												</li>
												<td>
													<li class="visible-xs visible-sm">
														<div class="product-price subtotal">Rp. <?= number_format($value->total,0,",","."); ?></div>
													</li>
												</td>
												<td></td>
												<td align="right">
													<div class="product-price unit-price">
														<b>Total :</b>
													</div>
												</td>
												<td colspan="">
													<div class="product-price subtotal" align="right">
														Rp. <?= number_format($value->total,0,",","."); ?>
													</div>
												</td>
												<td></td>
											</tr>
										</tfoot>
										<tr>
										</tfoot>
									</tbody>
								</table>
							</div>
						</div>

					</div>
				</div>
			</div>	
		<?php endforeach ?>


		<?php $this->load->view('footer'); ?>
