<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('header'); ?>

<div class="breadcrumb">
			<div class="container">
				<ul>
					<li><a href="<?= base_url() ?>">Home</a></li>
					<li>Profil</li>
					<li>Edit Profil</li>
				</ul>
			</div>
		</div>
		<!-- Content -->
		<div id="pageContent">
			<div class="container offset-14">
				<h1 class="block-title large">Edit Profil</h1>
				<div class="row">
					<div class="col-sm-8 col-sm-push-2 col-lg-6 col-lg-push-3">
						<div class="login-form-box">
							<?php 
                       if (isset($message)) { ?>
                        <div class="alert alert-success">                           
                            <strong>Success!</strong> Anda berhasil membuat akun.
                        </div>
                       <?php 
                        unset($message); } ?>                        
                        <?php 
                       if (isset($warning)) { ?>
                        <div class="alert alert-danger">
                             <strong>Email sudah digunakan!</strong> silahkan guanakan email lain
                        </div>
                       <?php 
                        unset($warning); } ?>

                        <?php foreach ($users as $var): ?>
                        	<h2 class="text-uppercase">Informasi Pribadi</h2>
							<form action="<?php echo site_url('controlleruser/editprofil')?>" method="post">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">
										<span class="icon icon-person_outline"></span>
										</span>
										<input type="text" class="form-control" name="nama" required="" value="<?= $var->nama ?>"/>
									</div>
								</div>
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">
										<span class="icon icon-email"></span>
										</span>
										<input type="email" class="form-control" name="email" required="" value="<?= $var->email ?>" readonly/>
									</div>
								</div>
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">
										<span class="icon icon-lock_outline"></span>
										</span>
										<input required="" type="password" id="LoginFormPass1" class="form-control" placeholder="Password" name="password" 
										>
									</div>
								</div>
								<input type="hidden" name="id_user" value="<?= $var->id_user ?>">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">
										<span class="icon icon-phone"></span>
										</span>
										<input type="number" class="form-control" name="nohp" required="" value="<?= $var->no_hp ?>"/>
									</div>
								</div>


								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">
										<span class="icon icon-home"></span>
										</span>
										<input type="text" class="form-control" name="alamat" required="" value="<?= $var->alamat ?>"/>
									</div>
								</div>
								<?php if ($this->session->userdata('role')=='Admin'): ?>
									<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">
										<span class="icon icon-person"></span>
										</span>
										<input type="text" class="form-control" name="role" required="" value="<?= $var->role ?>"/>
									</div>
								</div>
								<?php endif ?>
								<div class="row">

									<div class="col-md-12">
										<div class="button-block">
											<button type="submit" class="btn">EDIT PROFIL</button>
										</div>
									</div>
								</div>
							</form>
                        <?php endforeach ?>
							
						</div>
					</div>
				</div>
			</div>
		</div>
<?php $this->load->view('footer'); ?>