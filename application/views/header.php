<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Tokoikan.id</title>
	<meta name="keywords" content="HTML5 Template">
	<meta name="description" content="MyShop - Responsive HTML5 Template">
	<meta name="author" content="etheme.com">
	<link rel="shortcut icon" href="<?= base_url() ?>res/favicon.png">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="<?= base_url() ?>res/external/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" href="<?= base_url() ?>res/external/slick/slick.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>res/external/rs-plugin/css/settings.min.css" media="screen" />
	<link rel="stylesheet" href="<?= base_url() ?>res/external/magnific-popup/magnific-popup.css">
	<link rel="stylesheet" href="<?= base_url() ?>res/css/template.css">
	<link rel="stylesheet" href="<?= base_url() ?>res/font/icont-fonts.min.css">


</head>
<body>
	<div class="loader-wrapper">
		<div class="loader">
			<svg class="circular" viewBox="25 25 50 50">
				<circle class="loader-animation" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
			</svg>
		</div>
	</div>
	<nav class="panel-menu">
		<ul>
			<li><a href="index-rtl.html">HOME</a></li>
			<li><a href="index-rtl.html">TRANSAKSI</a></li>
			<li><a href="index-rtl.html">PROFIL SAYA</a></li>
		</ul>
		<div class="mm-navbtn-names" style="display:none">
			<div class="mm-closebtn">CLOSE</div>
			<div class="mm-backbtn">BACK</div>
		</div>
	</nav>
	<header>
		<!-- mobile-header -->
		<div class="mobile-header">
			<div class="container-fluid">
				<div class="pull-right">
					<!-- account -->
					<div class="account dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown">
							<span class="icon icon-person "></span>
							<span class="dropdown-label hidden-sm hidden-xs">Akun Saya</span>
						</a>
						<ul class="dropdown-menu">
							<li><a href="<?= site_url('controlleruser/viewprofil') ?>"><span class="icon icon-person"></span>Profil</a></li>

							<li><a href="#" data-toggle="modal" data-target="#modalLoginForm"><span class="icon icon-lock_outline"></span>Log In</a></li>
							<li><a href="<?= site_url('controlleruser/viewregister') ?>"><span class="icon icon-person_add"></span>Register</a></li>
							<li><a href="<?= site_url('controlleruser/logout') ?>"><span class="fa fa-sign-out"></span>Logout</a></li>
						</ul>
					</div>
					<!-- /account -->
					<!-- cart -->
					<div class="mobile-parent-cart"></div>
					<!-- /cart -->
				</div>
			</div>
			<div class="container-fluid">
				<!-- logo -->
				<div class="logo">
					<a href="<?= base_url()?>"><img src="<?= base_url() ?>res/images/logo-mobile.png" alt=""/></a>
				</div>
				<!-- /logo -->
			</div>
			<div class="container-fluid top-line">
				<div class="pull-left">
					<div class="mobile-parent-menu">
						<div class="mobile-menu-toggle">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="menu-text">
								MENU
							</span>
						</div>
					</div>
				</div>
				<div class="pull-right">
					<!-- search -->
					<div class="search">
						<a href="#" class="search-open"><span class="icon icon-search"></span></a>
						<div class="search-dropdown">
							<form action="<?= site_url('controllerikan/pencarian') ?>">
								<div class="input-outer">
									<input type="search" name="keyword" value="" maxlength="128" placeholder="Enter keyword">
									<button type="submit" class="btn-search"><span>Cari</span></button>
								</div>
								<a href="<?= base_url() ?>res/#" class="search-close"><span class="icon icon-close"></span></a>
							</form>
						</div>
					</div>
					<!-- /search -->
				</div>
			</div>
		</div>
		<!-- /mobile-header -->
		<!-- desktop-header -->
		<div class="desktop-header  header-06">
			<div class="container">
			</div>
			<div class="top-line">
				<div class="container">
					<div class="pull-left">
						
						<div class="logo">
							<a href="<?= base_url() ?>"><img src="<?= base_url() ?>res/images/logo.png" alt=""/></a>
						</div>
						
					</div>
					
					<div class="pull-right" >
						<!-- box-info -->
						<div class="box-info" style="margin-right: 45px">
							<div class="telephone">
								<span class="icon icon-call"></span>0858-0726-6262 <br>
								<div class="time">
									<label style="max-height: 8px">Buka Senin - Minggu, Jam 08:00 - 20:00</label>
								</div>
							</div>

						</div>

						<?php if ($this->session->userdata('login')): ?>
							<div class="box-info" style="margin-right: 5px">
								<a href="<?= site_url('controllerorderikan/viewkeranjangbelanja') ?>" class="box-compare">
									<span class="badge badge-cart"><?= $this->session->userdata('keranjang'); ?></span>
									<span style="120%" class="icon icon-shopping_basket"></span>
									<span class="title">KERANJANG</span>
								</a>
							</div>


							<!-- /box-info -->
							<!-- box-wishlist -->

							<!-- /box-wishlist -->
							<!-- box-compare -->

							<!-- /box-compare -->
							<!-- cart -->


							<!-- <div class="main-parent-cart">
								<div class="cart">
									<div class="dropdown">
										<a class="dropdown-toggle">
											<span class="icon icon-shopping_basket"></span>
											<span class="badge badge-cart">2</span>
											<div class="dropdown-label hidden-sm hidden-xs">KERANJANG</div>
										</a>
										<div class="dropdown-menu slide-from-top">
											<div class="container">
												<a href="#" class="icon icon-close cart-close"></a>
												<h4 class="empty-cart-js hide">Your Cart is Empty</h4>
												<div class="cart-bottom">
													<div class="pull-left">
														<a href="shopping_cart_01.html" class="btn icon-btn-left"><span class="icon icon-shopping_basket"></span>LIHAT KERANJANG CART</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div> -->


							<div class="account dropdown"">
								<a class="dropdown-toggle" data-toggle="dropdown">
									<span class="icon icon-person "></span>
									<span class="dropdown-label"><strong><?= $this->session->userdata('nama'); ?><?php if ($this->session->userdata('role')=='admin'): ?>
										( ADMIN ) 
									<?php endif ?></strong></span>
								</a>
								<ul class="dropdown-menu">
									<li><a href="<?= site_url('controlleruser/viewprofil') ?>"><span class="icon icon-person"></span>Profil</a></li>
									<li><a href="<?= site_url('controlleruser/logout') ?>"><span class="fa fa-sign-out"></span>Logout</a></li>
								</ul>
							</div>
						<?php else: ?>
							<div class="box-info" style="margin-right: 45px">
								<a href="#" data-toggle="modal" data-target="#modalLoginForm" class="box-compare">
									<span style="120%" class="icon icon-lock"></span>
									<span class="title">LOGIN</span>
								</a>
							</div>

							<div class="box-info" style="margin-right: 5px">
								<a href="<?= site_url('controlleruser/viewRegister')?>" class="box-compare">
									<span style="120%" class="icon icon-person_add"></span>
									<span class="title">REGISTER</span>
								</a>
							</div>
						<?php endif ?>
						

						
						<!-- /cart -->
					</div>
				</div>
			</div>
			<div class="top-line">
				<div class="container">
					<div class="pull-left">
						<div class="menu-parent-box">
							<!-- header-menu -->
							<nav class="header-menu">
								<uL>
									

									<li><a href="<?= base_url() ?>"><i class="fa fa-home"></i> HOME</a></li>
									<?php if ($this->session->userdata('role')=='admin'): ?>
										
									<?php else: ?>
										<li><a href="<?= base_url() ?>"> CARA PEMESANAN</a></li>
										<li><a href="<?= base_url() ?>"> METODE PEMBAYARAN</a></li>
										<li><a href="<?= base_url() ?>"> F.A.Q</a></li>
									<?php endif ?>
									<?php if ($this->session->userdata('login')): ?>
										<?php if ($this->session->userdata('role')=='admin'): ?>
											<li><a href="<?= site_url('controllerorderikan/viewdaftartransaksi') ?>">DAFTAR TRANSAKSI</a></li>
											<li><a href="<?= site_url('controlleruser/viewdaftaruser') ?>">DAFTAR USER</a></li>
											<li><a href="<?= site_url('controllerikan/viewdaftarpostikan') ?>"> DAFTAR IKAN</a></li>
										<?php else: ?>
											<li><a href="<?= site_url('controllerorderikan/viewtransaksi') ?>"><i style="" class="fa fa-cart-arrow-down"></i> TRANSAKSI</a></li>
											<li><a href="<?= site_url('controllerorderikan/viewkeranjangbelanja') ?>"><i style="" class="icon icon-shopping_basket"></i> KERANJANG ( <?= $this->session->userdata('keranjang'); ?> )</a></li>
										<?php endif ?>
										
										<li><a href="<?= site_url('controlleruser/logout') ?>"> <i style="120%" class="fa fa-power-off"></i> LOGOUT</a></li>
									<?php else: ?>
										
									<?php endif ?>
									
									


								</ul>
							</nav>
							<!-- /header-menu -->
						</div>
					</div>
					<div class="pull-right">
						<!-- search -->
						<div class="search">
							<a href="#" class="search-open"><span class="icon icon-search"></span></a>
							<div class="search-dropdown">
								<form action="<?= site_url('controllerikan/pencarian') ?>">
									<div class="input-outer">
										<input type="search" name="keyword" value="" maxlength="128" placeholder="Enter keyword">
										<button type="submit" class="btn-search"><span>Cari</span></button>
									</div>
									<a href="<?= base_url() ?>res/#" class="search-close"><span class="icon icon-close"></span></a>
								</form>
							</div>
						</div>
						<!-- /search -->
					</div>
				</div>
			</div>			
		</div>
		<!-- /desktop-header -->
		<!-- stuck nav -->
		<div class="stuck-nav" >
			<div class="container">
				<div class="pull-left">
					<div class="stuck-menu-parent-box"></div>
				</div>
				<div class="pull-right">
					<div class="stuck-cart-parent-box"></div>
				</div>
			</div>
		</div>
		<!-- /stuck nav -->
	</header>

	<div class="modal  fade"  id="modalLoginForm" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-md-small">
			<div class="modal-content ">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>

					<h4 class="modal-title text-center text-uppercase"><span style="120%" class="icon icon-lock"></span> Form Login</h4>
					<?php
					if($this->session->flashdata('logingagal')) { ?>
					<div class="alert alert-danger">
						<strong>Username atau Password salah!</strong> silahkan login kembali..
					</div>
					<?php } ?>
					<?php
					if($this->session->flashdata('logindulu')) { ?>
					<div class="alert alert-danger">
						<strong>Mohon Maaf!</strong> Untuk membeli ikan anda harus login dahulu
					</div>
					<?php } ?>
				</div>
				<form action="<?= site_url('controlleruser/login') ?>" method="post">
					<div class="modal-body">
						<!--modal-add-login-->
						<div class="modal-login">
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="icon icon-person_outline"></span>
									</span>
									<input type="email" id="LoginFormName" class="form-control" placeholder="Email:" name="email">
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="icon icon-lock_outline"></span>
									</span>
									<input type="password" id="LoginFormPass" class="form-control" placeholder="Password:"  name="password">
								</div>
							</div>
							<div class="checkbox-group">
								<input type="checkbox" id="checkBox2">
								<label for="checkBox2">
									<span class="check"></span>
									<span class="box"></span>
									Remember me
								</label>
							</div>
							<button type="submit" class="btn btn-full">LOGIN</button>
							<a href="<?= site_url('controlleruser/viewRegister') ?>"></a><button type="button" class="btn btn-full">REGISTER</button>
							<div class="text-center"></div>
						</div>
						<!--/modal-add-login-->
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>