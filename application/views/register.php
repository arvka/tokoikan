<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Obaju e-commerce template">
    <meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
    <meta name="keywords" content="">

    <title>
        Obaju : e-commerce template
    </title>

    <meta name="keywords" content="">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="<?php echo base_url(); ?>res/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>res/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>res/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>res/css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>res/css/owl.theme.css" rel="stylesheet">

    <!-- theme stylesheet -->
    <link href="<?php echo base_url(); ?>res/css/style.default.css" rel="stylesheet" id="theme-stylesheet">

    <!-- your stylesheet with modifications -->
    <link href="<?php echo base_url(); ?>res/css/custom.css" rel="stylesheet">

    <script src="<?php echo base_url(); ?>res/js/respond.min.js"></script>

    <link rel="shortcut icon" href="favicon.png">



</head>

<body>
    <?php $this->load->view("component/header"); ?>
    <div id="all">

        <div id="content">
            <div class="container">

                <div class="col-md-12">

                    <ul class="breadcrumb">
                        <li><a href="#">Home</a>
                        </li>
                        <li>Register</li>
                    </ul>

                </div>


                    <div class="col-md-6 col-md-offset-3">
                    <div class="box">
                        <h1>New account</h1>
                        <p>Registrasi sekarang untuk menjadi member dan dapatkan keuntungannya !</p>
                        <hr>

                         <?php 
                       if (isset($message)) { ?>
                        <div class="alert alert-success">                           
                            <strong>Success!</strong> Anda berhasil membuat akun.
                        </div>
                       <?php 
                        unset($message); } ?>
                        
                        <?php 
                       if (isset($warning)) { ?>
                        <div class="alert alert-danger">
                             <strong>Email sudah digunakan!</strong> silahkan guanakan email lain
                        </div>
                       <?php 
                        unset($warning); } ?>
                

                        <?php echo form_open('usercontroller/register'); ?>

                            <div class="form-group">
                                <label for="name">Nama</label>
                                <input type="text" class="form-control" name="nama" required="" value="<?php echo set_value('nama'); ?>"/>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="email" required="" value="<?php echo set_value('email');?>"/>
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" name="password" required="" value="<?php echo set_value('password'); ?>"/>
                            </div>
                            <div class="form-group">
                                <label for="no_hp">No. HP</label>
                                <input type="number" class="form-control" name="nohp" required="" value="<?php echo set_value('nohp'); ?>"/>
                            </div>
                            <div class="form-group">
                                <label for="alamat">Alamat</label>
                                <input type="text" class="form-control" name="alamat" required="" value="<?php echo set_value('alamat'); ?>"/>
                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-user-md"></i> Register</button>
                            </div>
                        </form>
                    </div>
                </div>


            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->

        <?php $this->load->view('component/footer');?>
    </div>
    <!-- /#all -->


    

    <!-- *** SCRIPTS TO INCLUDE ***
 _________________________________________________________ -->
    <script src="<?php echo base_url(); ?>res/js/jquery-1.11.0.min.js"></script>
    <script src="<?php echo base_url(); ?>res/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>res/js/jquery.cookie.js"></script>
    <script src="<?php echo base_url(); ?>res/js/waypoints.min.js"></script>
    <script src="<?php echo base_url(); ?>res/js/modernizr.js"></script>
    <script src="<?php echo base_url(); ?>res/js/bootstrap-hover-dropdown.js"></script>
    <script src="<?php echo base_url(); ?>res/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url(); ?>res/js/front.js"></script>



</body>

</html>
