<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class IkanModel extends CI_Model {

	function setDataPostIkan($data) {
		$this->db->insert('ikan', $data);
	}

	function getDataAllIkan() {
		return $this->db->get('ikan')->result();
	}

	function getDataCariIkan ($keyword){
		$this->db->select('*');
		$this->db->from('ikan');
		$this->db->like('nama_ikan', $keyword);
		$query = $this->db->get();
		$data['data'] = $query->result();
		$data['hasil'] = 'ada' ;
		if($query->num_rows() == 0) {
			$data['hasil'] = 'kosong';
		}
		return $data;
	}

	public function deleteIkan($id_ikan){
		$this->db->delete('ikan', array('id_ikan' => $id_ikan));
	}

	function getDataPostIkan($id_ikan) {
		$query = $this->db->get_where('ikan', array('id_ikan' => $id_ikan))->result();
		return $query;
	}

	public function updateDataPostIkan($data){

		$this->db->update('ikan', $data, array('id_ikan' => $data['id_ikan']));

	}
}