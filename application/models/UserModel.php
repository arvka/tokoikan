<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model {

	
	function setDataUser($data)
	{
		$this->db->insert('user', $data);
	}

	
	public function getAuth($email,$password)
	{
		$query = $this->db->get_where('user',array('email'=>$email,'password' => md5($password)));

		if($query->num_rows() == 1) {
			$row  = $this->db->query('SELECT * FROM user where email = "'.$email.'"');
			$data = $row->row();
			$id   = $data->id_user;
			$nama = $data->nama;
			$role = $data->role;
			$email = $data->email;  

			$this->session->set_userdata('id_login', uniqid(rand()));
			$this->session->set_userdata('id_user', $id);
			$this->session->set_userdata('role', $role);
			$this->session->set_userdata('nama', $nama);
			$this->session->set_userdata('email', $email);
			$this->session->set_userdata('login', 'yes');
			
			
			return 'sukses';

		}else{
			$this->session->set_flashdata('logingagal','Username atau password anda salah, silakan coba lagi.. ');
			return 'gagal';
		}

		
	}

	public function getDataUser($id_user)
	{
		$query = $this->db->get_where('user', array('id_user' => $id_user));

		return $query;
	}

	public function getDataAllUser()
	{
		$query = $this->db->get('user')->result();

		return $query;
	}

	public function updateDataUser($data, $id_user){

		$this->db->update('user', $data, array('id_user' => $id_user));

	}

	public function deleteUser($id_user){
		$this->db->delete('user', array('id_user' => $id_user));

	}
}