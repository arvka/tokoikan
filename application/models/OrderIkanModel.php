<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OrderIkanModel extends CI_Model {

    function getDataKeranjangBelanja(){
        $idUser = $this->session->userdata('id_user');
        $sql = $this->db->query('SELECT isi_keranjang.id_isi_keranjang, ikan.path_gambar,ikan.nama_ikan,isi_keranjang.jumlah,ikan.harga, isi_keranjang.jumlah*ikan.harga as total FROM keranjang INNER JOIN isi_keranjang ON keranjang.id_keranjang = isi_keranjang.id_keranjang INNER JOIN ikan ON ikan.id_ikan = isi_keranjang.id_ikan WHERE id_user=  "'.$idUser.'" AND keranjang.status ="dipesan" ' );
        $data["keranjang"] = $sql->result();
        $total = $this->db->query('SELECT keranjang.id_keranjang, sum(isi_keranjang.jumlah*ikan.harga) as total FROM keranjang INNER JOIN isi_keranjang ON keranjang.id_keranjang = isi_keranjang.id_keranjang INNER JOIN ikan ON ikan.id_ikan = isi_keranjang.id_ikan WHERE id_user = "'.$idUser.'" AND keranjang.status ="dipesan"');
        $data["totalJumlah"] = $total->result();
        $this->session->set_userdata('keranjang', $sql->num_rows());
        return $data;
    }
    
    function getDatarTransaksi(){
        $idUser = $this->session->userdata('id_user');
        $sqlString = "SELECT transaksi.id_transaksi as id, user.nama as nama , sum(isi_keranjang.jumlah*ikan.harga) as total, transaksi.status_transaksi as status FROM keranjang INNER JOIN isi_keranjang ON keranjang.id_keranjang = isi_keranjang.id_keranjang INNER JOIN ikan ON ikan.id_ikan = isi_keranjang.id_ikan INNER JOIN transaksi ON keranjang.id_keranjang = transaksi.id_keranjang INNER JOIN user ON user.id_user = keranjang.id_user GROUP BY transaksi.id_transaksi";
        $sql = $this->db->query($sqlString);
        $data = $sql->result_array();
        return $data;
    }

    function setIsiKeranjang($id_user, $datapesan){
     $query= $this->db->get_where('keranjang',array('id_user'=>$id_user , 'status'=> 'dipesan'));
       if($query->num_rows() == 0) { //jika kosong buat keranjang keranjang
        $data = array(
            'id_user'  => $id_user,
            'status'  => 'dipesan'
        );
        $queryset = $this->db->insert('keranjang', $data);
        $query= $this->db->get_where('keranjang',array('id_user'=>$id_user , 'status'=> 'dipesan'));
    }
    $data = $query->row();
    $datapesan['id_keranjang'] = $data->id_keranjang;
    $queryset = $this->db->insert('isi_keranjang', $datapesan);
}


    function setDataTransaksi($data){
        $status ['status'] = 'dibayar';
       $this->db->update('keranjang', $status , array('id_keranjang' => $data['id_keranjang']));
       $query = $this->db->insert('transaksi', $data);
       return $query;
    }

    function deleteIsiKeranjang($idIsiKeranjang){        
       $this->db->delete('isi_keranjang', array('id_isi_keranjang' => $idIsiKeranjang));
       $this->session->set_flashdata('hapus', 'berhasil');
       
    }

    function getDataTransaksi(){
        $idUser = $this->session->userdata('id_user');
        $sql = $this->db->query('SELECT user.id_user, keranjang.id_keranjang, transaksi.id_transaksi as id_transaksi, user.nama as nama , sum(isi_keranjang.jumlah*ikan.harga) as total, transaksi.status_transaksi as status, transaksi.alamat, transaksi.no_hp , transaksi.tgl_create, transaksi.path_bukti_pembayaran FROM keranjang INNER JOIN isi_keranjang ON keranjang.id_keranjang = isi_keranjang.id_keranjang INNER JOIN ikan ON ikan.id_ikan = isi_keranjang.id_ikan INNER JOIN transaksi ON keranjang.id_keranjang = transaksi.id_keranjang INNER JOIN user ON user.id_user = keranjang.id_user WHERE user.id_user=  "'.$idUser.'" GROUP BY transaksi.id_transaksi  ' );
        $sql2 = $this->db->query('SELECT keranjang.id_keranjang, isi_keranjang.id_isi_keranjang, ikan.path_gambar,ikan.nama_ikan,isi_keranjang.jumlah,ikan.harga, isi_keranjang.jumlah*ikan.harga as total FROM keranjang INNER JOIN isi_keranjang ON keranjang.id_keranjang = isi_keranjang.id_keranjang INNER JOIN ikan ON ikan.id_ikan = isi_keranjang.id_ikan WHERE id_user=  "'.$idUser.'" ' );
        $total = $this->db->query('SELECT keranjang.id_keranjang, sum(isi_keranjang.jumlah*ikan.harga) as total FROM keranjang INNER JOIN isi_keranjang ON keranjang.id_keranjang = isi_keranjang.id_keranjang INNER JOIN ikan ON ikan.id_ikan = isi_keranjang.id_ikan WHERE id_user = "'.$idUser.'" GROUP BY keranjang.id_keranjang');
        $data['transaksi'] = $sql->result();
        $data['keranjang'] = $sql2->result();
        $data['total'] = $total->result();
        return $data;
    }

    function getDataDaftarTransaksi(){
        $idUser = $this->session->userdata('id_user');
        $sql = $this->db->query('SELECT user.id_user, keranjang.id_keranjang, transaksi.id_transaksi as id_transaksi, user.nama as nama , sum(isi_keranjang.jumlah*ikan.harga) as total, transaksi.status_transaksi as status, transaksi.alamat, transaksi.no_hp , transaksi.tgl_create, transaksi.path_bukti_pembayaran FROM keranjang INNER JOIN isi_keranjang ON keranjang.id_keranjang = isi_keranjang.id_keranjang INNER JOIN ikan ON ikan.id_ikan = isi_keranjang.id_ikan INNER JOIN transaksi ON keranjang.id_keranjang = transaksi.id_keranjang INNER JOIN user ON user.id_user = keranjang.id_user GROUP BY transaksi.id_transaksi  ' );
        $sql2 = $this->db->query('SELECT keranjang.id_keranjang, isi_keranjang.id_isi_keranjang, ikan.path_gambar,ikan.nama_ikan,isi_keranjang.jumlah,ikan.harga, isi_keranjang.jumlah*ikan.harga as total FROM keranjang INNER JOIN isi_keranjang ON keranjang.id_keranjang = isi_keranjang.id_keranjang INNER JOIN ikan ON ikan.id_ikan = isi_keranjang.id_ikan ' );
        $total = $this->db->query('SELECT keranjang.id_keranjang, sum(isi_keranjang.jumlah*ikan.harga) as total FROM keranjang INNER JOIN isi_keranjang ON keranjang.id_keranjang = isi_keranjang.id_keranjang INNER JOIN ikan ON ikan.id_ikan = isi_keranjang.id_ikan GROUP BY keranjang.id_keranjang');
        $data['transaksi'] = $sql->result();
        $data['keranjang'] = $sql2->result();
        $data['total'] = $total->result();
        return $data;
    }

    public function updateDataTransaksi($data){
        $query = $this->db->update('transaksi', $data, array('id_transaksi' => $data['id_transaksi']));
        return $query;
    }

}

/* End of file OrderIkanModel.php */
/* Location: ./application/models/OrderIkanModel.php */