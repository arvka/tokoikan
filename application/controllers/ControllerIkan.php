<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ControllerIkan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{

		redirect('controllerikan/viewhome');
	}

	public function viewHome()
	{
		$dataIkan["data"] = $this->ikanmodel->getDataAllIkan();
		$this->session->set_flashdata('home','homeee');
		$this->load->view('viewHome', $dataIkan);

	}

	public function viewPostingIKan() {
		if ($this->session->userdata('login')) {
			
		} else {
			redirect('controllerikan');  
		}
		$pesan = $this->session->flashdata('pesan');
		$pesan2 = $this->session->flashdata('warning');
		$status['message'] = $pesan;
		$status['warning'] = $pesan2;
		$this->load->view('viewpostingIkan', $status);
	}

	public function postingIkan()
	{
		if ($this->session->userdata('login')) {
			
		} else {
			redirect('controllerikan');  
		}
		$data = array();
		$data['nama_ikan'] = $this->input->post('namaikan');
		$data['harga'] = $this->input->post('harga');
		$data['ketersedian'] = $this->input->post('ketersediaan');
		$data['tgl_create'] = $today = date("Y-m-d");  
		$data['tgl_update'] = $today = date("Y-m-d");  
		
		if(!empty($_FILES['foto']['name'])){ // Jika user menekan tombol Submit (Simpan) pada form
			// lakukan upload file dengan memanggil function upload yang ada di GambarModel.php
			$upload = array();
			$config['upload_path'] = realpath(APPPATH . '../res/img/product/');
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size'] = '2048';
			$config['remove_space'] = TRUE;
			$new_name = time().'_'.$_FILES["foto"]['name'];
			$config['file_name'] = $new_name;
			
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			// Load konfigurasi uploadnya
			if($this->upload->do_upload('foto')){ // Lakukan upload dan Cek jika proses upload berhasil
				// Jika berhasil :
				$upload = array('result' => 'success','file' => $this->upload->data(), 'error' => '');
				$data['path_gambar'] = $upload['file']['file_name'];
			}else{
				$upload = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
			}
			if($upload['result'] == "success"){
				$this->ikanmodel->setDatapostIkan($data);
				$this->session->set_flashdata('postikan','Anda berhasil postikan');
				// Redirect kembali ke halaman awal / halaman view data
				$this->load->view('viewpostingIkan');
			}else{ // Jika proses upload gagal
				$status['message'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
				echo $status['message'];
				$this->load->view('viewpostingIkan');
			}
		}else {
			$this->ikanmodel->setDatapostIkan($data);
			$this->session->set_flashdata('postikan','Anda berhasil postikan');
			$this->load->view('viewpostingIkan');
		}
	}
	
	public function editpostIkan()
	{
		if ($this->session->userdata('login')) {
			
		} else {
			redirect('controllerikan');  
		}
		$data = array();
		$data['nama_ikan'] = $this->input->post('namaikan');
		$data['harga'] = $this->input->post('harga');
		$data['ketersedian'] = $this->input->post('ketersediaan');
		$data['id_ikan'] =  $this->input->post('id_ikan');
		
		if(!empty($_FILES['foto']['name'])){ // Jika user menekan tombol Submit (Simpan) pada form
			// lakukan upload file dengan memanggil function upload yang ada di GambarModel.php
			$upload = array();
			$config['upload_path'] = realpath(APPPATH . '../res/img/product/');
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size'] = '2048';
			$config['remove_space'] = TRUE;
			$new_name = time().'_'.$_FILES["foto"]['name'];
			$config['file_name'] = $new_name;
			
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			// Load konfigurasi uploadnya
			
			if($this->upload->do_upload('foto')){ // Lakukan upload dan Cek jika proses upload berhasil
				// Jika berhasil :
				$upload = array('result' => 'success','file' => $this->upload->data(), 'error' => '');
				$data['path_gambar'] = $upload['file']['file_name'];
			}else{
				$upload = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
			}
			if($upload['result'] == "success"){
				$this->ikanmodel->UpdateDataPostIkan($data);
				$this->session->set_flashdata('updateikan','Anda berhasil update postikan');
				// Redirect kembali ke halaman awal / halaman view data
				redirect('controllerikan/viewDaftarPostIkan');
			}else{ // Jika proses upload gagal
				$status['message'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
				echo $status['message'];
				$this->load->view('viewpostingIkan');
			}
		}else {
			$this->ikanmodel->UpdateDataPostIkan($data);
			$this->session->set_flashdata('postikan','Anda berhasil postikan');
			redirect('controllerikan/viewDaftarPostIkan');
		}
	}

public function pencarian()
{
	$keyword = $this->input->get('keyword');
	$data= $this->ikanmodel->getDataCariIkan($keyword);
	$data["keyword"] = $keyword;
	$this->load->view('viewPencarian', $data);
}

public function viewDaftarPostIkan() {
	$dataIkan["ikan"] = $this->ikanmodel->getDataAllIkan();
	$this->load->view('viewDaftarPostIkan', $dataIkan);
}

public function hapusPostIkan($id_ikan) {
	if ($this->session->userdata('login')) {

	} else {
		redirect('controllerikan');  
	}
	$data["user"] = $this->ikanmodel->deleteIkan($id_ikan);
	$this->session->set_flashdata('sukseshapus', 'value');
	redirect('controllerikan/viewDaftarPostIkan');
}

public function viewEditPostIkan($id_ikan) {
	$dataIkan["ikan"] = $this->ikanmodel->getDataPostIkan($id_ikan);
	$this->load->view('viewEditPostIkan', $dataIkan);
}

}
