<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ControllerUser extends CI_Controller {

	public function index()
	{
		
	}

	function __construct(){
		parent::__construct();
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form', 'url'));
		$this->load->model('usermodel');
	}

	public function viewRegister(){
		$pesan=$this->session->flashdata('pesan');
		$pesan2=$this->session->flashdata('warning');
		$status['message'] = $pesan;
		$status['warning'] = $pesan2;
		$this->load->view('viewregister',$status);
	}

	public function register() {


		$this->form_validation->set_rules('email', 'Email', 'is_unique[user.email]'
	);

		if($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('warning','emailsama');		
			redirect('controlleruser/viewRegister');
		}

		else {


			$data['nama']   =    $this->input->post('nama');
			$data['email']  =    $this->input->post('email');
			$data['password'] =    md5($this->input->post('password'));
			$data['alamat'] 	= $this->input->post('alamat');
			$data['no_hp'] 		= $this->input->post('nohp');

			$this->usermodel->setDataUser($data);
			
			$this->session->set_flashdata('pesan','berhasil');
			redirect('controlleruser/viewRegister');
		}
	}

	public function viewLogin()
	{
		$this->load->view('login');
	}

	public function login() {
		$username = $this->input->post('email');  
		$password = $this->input->post('password'); 
		$keranjang = $status = $this->orderikanmodel->getDataKeranjangBelanja();
		$status = $this->usermodel->getAuth($username, $password);
		if ($status=='sukses') {
			redirect(base_url()) ; 	
		}
		else {
			redirect('controllerikan/viewHome');
		}	  
	}  

	public function logout(){  
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('id_login');
		$this->session->unset_userdata('id');
		$this->session->set_flashdata('logout','Anda berhasil logout');
		$this->session->unset_userdata('login');
		$this->session->unset_userdata('role');
		
		redirect('controllerikan');  
	}

	public function viewProfil()
	{
		if ($this->session->userdata('login')) {
			$id_user = $this->session->userdata('id_user');
			$data['users'] = $this->usermodel->getDataUser($id_user)->result();
			$this->load->view('viewprofil',$data);
		} else {
			redirect('controllerikan');  
		}
		
	} 


	public function viewEditProfil()
	{
		if ($this->session->userdata('login')) {
			$id_user = $this->session->userdata('id_user');
			$data['users'] = $this->usermodel->getDataUser($id_user)->result();
			$this->load->view('vieweditprofil',$data);
		} else {
			redirect('controllerikan');  
		}
		
	}

	public function viewEditProfilAdmin($id_user)
	{
		if ($this->session->userdata('login')) {
			$data['users'] = $this->usermodel->getDataUser($id_user)->result();
			$this->session->set_flashdata('editbyadmin','value');
			$this->load->view('vieweditprofil',$data);
		} else {
			redirect('controllerikan');  
		}
		
	}

	public function editProfil()
	{
		if ($this->session->userdata('login')) {
			
		} else {
			redirect('controllerikan');  
		}
		$data = array();

		$data['nama']   =    $this->input->post('nama');
		$data['email']  =    $this->input->post('email');
		$data['password'] =    md5($this->input->post('password'));
		$data['alamat'] 	= $this->input->post('alamat');
		$data['no_hp'] 		= $this->input->post('nohp');
		$data['id_user'] 		= $this->input->post('id_user');
		$data['role'] =  $this->input->post('role');
		

    if(!empty($_FILES['foto']['name'])){ // Jika user menekan tombol Submit (Simpan) pada form
      // lakukan upload file dengan memanggil function upload yang ada di GambarModel.php
    	$upload = array();
    	$image_path = realpath(APPPATH . '../upload/fotouser');
    	$config['upload_path']          = $image_path;
    	$config['allowed_types']        = 'gif|jpg|png';
    	$config['remove_spaces'] = TRUE;
    	$new_name = time().'_'.$_FILES["foto"]['name'];
    	$config['file_name'] = $new_name;

    	$this->load->library('upload', $config);
    	$this->upload->initialize($config);
     // Load konfigurasi uploadnya
    if($this->upload->do_upload('foto')){ // Lakukan upload dan Cek jika proses upload berhasil
      // Jika berhasil :
    	
    	$upload = array('result' => 'success','file' => $this->upload->data(), 'error' => '');
    	$data['path_foto'] = $upload['file']['file_name'];

    }else{
    	$upload = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
    	
    }


    if($upload['result'] == "success"){
    	
    	$id_user = $this->session->userdata('id_user');
    	$this->usermodel->updateDataUser($data,$data['id_user']);
    	$this->session->set_flashdata('updateprofil','Anda berhasil update profil');
         // Redirect kembali ke halaman awal / halaman view data
    	if ($this->session->flashdata('editbyadmin')) {
    		redirect('controlleruser/viewDaftarUser');
    	} else {
    		redirect('controlleruser/viewProfil');
    	}
    	
    }else
      { // Jika proses upload gagal
        $status['message'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
        echo $status['message'];
        
    }

}
else {
	$id_user = $this->session->userdata('id_user');
	$this->usermodel->updateDataUser($data, $data['id_user']);
	$this->session->set_flashdata('updateprofil','Anda berhasil update profil');
	if ($this->session->flashdata('editbyadmin')) {
		$this->session->set_flashdata('suksesedit', 'value');
		redirect('controlleruser/viewDaftarUser');
	} else {
		redirect('controlleruser/viewProfil');
	}

}

}

public function viewDaftarUser() {
	if ($this->session->userdata('login')) {

	} else {
		redirect('controllerikan');  
	}
	$data["user"] = $this->usermodel->getDataAllUser();
	$this->load->view('ViewDaftarUser', $data);
}

public function hapusUser($id_user) {
	if ($this->session->userdata('login')) {

	} else {
		redirect('controllerikan');  
	}
	$data["user"] = $this->usermodel->deleteUser($id_user);
	$this->session->set_flashdata('sukseshapus', 'value');
	redirect('controlleruser/viewDaftarUser');
}


}