<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of OrderIkanController
 *
 * @author HP
 */
class OrderIkanController extends CI_Controller {
    function viewKeranjangBelanja(){
        $temp = $this->orderikanmodel->getDataKeranjangBelanja();
        $data["keranjang"] = $temp["keranjang"];
        $data["totalKeranjang"] = $temp["totalJumlah"];
        $this->load->view('ViewKeranjangBelanja',$data);
    }
    function viewDaftarTransaksi(){
        $temp = $this->orderikanmodel->getDataTransaksi();
        $data["dTransaksi"] = $temp;
        $this->load->view('ViewDaftarTransaksi',$data);
    }
    
    function viewStatusTransaksi(){
        $temp = $this->orderikanmodel->getStatusTransaksi();
        $data["dStatusTransaksi"] = $temp;
        $this->load->view('ViewStatusTransaksi',$data);
//        print_r($data);
    }
    
    function unggahBuktiPembayaran(){
        $bukti = '';
        if (!empty($_FILES['gambarBukti']['name'])) { // Jika user menekan tombol Submit (Simpan) pada form
            // lakukan upload file dengan memanggil function upload yang ada di GambarModel.php
            $upload = array();
            $image_path = realpath(APPPATH . '../upload/fotoBukti');
            $config['upload_path'] = $image_path;
            $config['allowed_types'] = 'gif|jpg|png';
            $config['remove_spaces'] = TRUE;
            $new_name = time() . '_' . $_FILES["gambarBukti"]['name'];
            $config['file_name'] = $new_name;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            // Load konfigurasi uploadnya
            if ($this->upload->do_upload('gambarBukti')) { // Lakukan upload dan Cek jika proses upload berhasil
                // Jika berhasil :
                $upload = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
                $bukti = $upload['file']['file_name'];
            } else {
                $upload = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
            }
            if ($upload['result'] == "success") {
                $idTransaksi = $_REQUEST['idTransaksi'];
                $this->orderikanmodel->setBuktiPembayaran($bukti, $idTransaksi);
                $this->session->set_flashdata('sukses', 'Anda berhasil mengirim foto bukti transaksi');
                // Redirect kembali ke halaman awal / halaman view data
                $data = array("success","Bukti transaksi berhasil dikirim, harap tunggu untuk proses verifikasi");
                redirect('orderikancontroller/viewStatusTransaksi');
            } else { // Jika proses upload gagal
                $status['message'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
                $this->session->set_flashdata('fail','Anda gagal mengirim foto bukti transaksi');
                redirect('orderikancontroller/viewStatusTransaksi');
                echo $status['message'];
            }
        } else {
            $this->usermodel->updateDataUser($data, $id_user);
            $this->session->set_flashdata('updateprofil', 'Anda berhasil update bukti transaksi');
            redirect('orderikancontroller/viewStatusTransaksi');
        }
    }
}
