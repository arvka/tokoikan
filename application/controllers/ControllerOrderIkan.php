<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of OrderIkanControlle
 *
 * @author HP
 */
class ControllerOrderIkan extends CI_Controller {
    function viewKeranjangBelanja(){
        if ($this->session->userdata('login')) {
            
        } else {
           redirect('controllerikan');  
        }
        $temp = $this->orderikanmodel->getDataKeranjangBelanja();
        $data["keranjang"] = $temp["keranjang"];
        $data["totalHarga"] = $temp["totalJumlah"];
        $this->load->view('ViewKeranjangBelanja', $data);
    }

    function viewStatusTransaksi(){
        $temp = $this->orderikanmodel->getDataTransaksi();
        $data["dTransaksi"] = $temp;
        $this->load->view('ViewDaftarTransaksi',$data);
    }
    
    function tambahKeranjang($id_ikan){
        if ($this->session->userdata('login')) {
            
        } else {
           $this->session->set_flashdata('logindulu', 'login dulu');
          redirect('controllerikan/viewHome');
        }
        //cari status keranjang user

        $datapesan['id_ikan'] = $id_ikan;
        $datapesan['jumlah'] = $this->input->post('jumlah');
        $id_user = $this->session->userdata('id_user');
        $temp = $this->orderikanmodel->setIsiKeranjang($id_user,$datapesan);
        $this->session->set_flashdata('tambahkeranjang', 'value');
        redirect('controllerorderikan/viewKeranjangBelanja');
        
    }

    function pembelian(){
        if ($this->session->userdata('login')) {
            
        } else {
           redirect('controllerikan');  
        }
        $data['id_keranjang'] = $this->input->post('id_keranjang');
        $data['no_hp'] = $this->input->post('nohp');
        $data['alamat'] = $this->input->post('alamat');

        $temp = $this->orderikanmodel->setDataTransaksi($data);
        redirect('controllerorderikan/viewtransaksi');

    }

    function hapusIsiKeranjang($idIsiKeranjang){        
        $temp = $this->orderikanmodel->deleteIsiKeranjang($idIsiKeranjang);
        redirect('controllerorderikan/viewkeranjangbelanja');  
    }

    function viewTransaksi(){
        if ($this->session->userdata('login')) {
            
        } else {
           redirect('controllerikan');  
        }
        $data= $this->orderikanmodel->getDataTransaksi();
        $this->load->view('viewTransaksi', $data); 
    }

    function editDataTransaksi(){
        if ($this->session->userdata('login')) {
            
        } else {
           redirect('controllerikan');  
        }
        $data['id_transaksi'] = $this->input->post('id_transaksi');
        $data['no_hp'] = $this->input->post('nohp');
        $data['alamat'] = $this->input->post('alamat');
        $temp = $this->orderikanmodel->updateDataTransaksi($data);
        redirect('controllerorderikan/viewtransaksi');
    }

    function uploadBuktiPembayaran(){
        if ($this->session->userdata('login')) {
            
        } else {
           redirect('controllerikan');  
        }
        $data['id_transaksi'] = $this->input->post('id_transaksi');
        $config['upload_path'] = realpath(APPPATH . '../res/img/buktibayar/');
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = '2048';
        $config['remove_space'] = TRUE;
        $new_name = time();
        $config['file_name'] = $new_name;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
     // Load konfigurasi uploadnya
    if($this->upload->do_upload('bukti_pembayaran')){ // Lakukan upload dan Cek jika proses upload berhasil
      // Jika berhasil :

        $upload = array('result' => 'success','file' => $this->upload->data(), 'error' => '');
        $data['path_bukti_pembayaran'] = $upload['file']['file_name'];
        
        $temp = $this->orderikanmodel->updateDataTransaksi($data);        
        redirect('controllerorderikan/viewtransaksi');

    }else{
        $upload = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
        echo $upload['error'];
    }
    
    }


    function viewDaftarTransaksi(){
     
        $data= $this->orderikanmodel->getDataDaftarTransaksi();
        $this->load->view('viewDaftarTransaksi', $data); 
    }

    function verifikasiPembayaran($id_transaksi, $status){
        if ($this->session->userdata('login')) {
            
        } else {
           redirect('controllerikan');  
        }
        $data['id_transaksi'] = $id_transaksi;
        $data['status_transaksi'] = $status;
        $temp = $this->orderikanmodel->updateDataTransaksi($data);

        if ($status=='pending') {
            $this->session->set_flashdata('batalverifikasi', 'value');    
        } else {
            $this->session->set_flashdata('verifikasi', 'value');
        }

        redirect('controllerorderikan/viewdaftartransaksi');
    }
}
