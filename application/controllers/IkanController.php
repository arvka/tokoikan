<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class IkanController extends CI_Controller {
	public function redirectPost(){
		$this->load->view('viewpostingIkan');
	}
	public function viewPost() {
        
        $pesan = $this->session->flashdata('pesan');
        $pesan2 = $this->session->flashdata('warning');
        $status['message'] = $pesan;
        $status['warning'] = $pesan2;
        $this->load->view('viewpostingIkan', $status);
    }

    public function viewDaftarPost() {
        $dataIkan["data"] = $this->ikanmodel->getDataAllIkan();
        $this->load->view('viewDaftarPost', $dataIkan);
    }

	public function postikan(){
		//gambar
		
		
		//data
		$data['nama_ikan'] = $this->input->post('inputIkan');
                $data['harga'] = $this->input->post('inputharga');
                $data['ketersedian'] = $this->input->post('ketersediaan');
                $data['tgl_create'] = $today = date("Y-m-d");  
                $data['tgl_update'] = $today = date("Y-m-d");  
		$upload = $this->upload($data['nama_ikan']);
                
		//$data['path_gambar']=$upload['file']['file_name'];
		
		if($upload['result'] == "success"){ // Jika proses upload sukses
			 // Panggil function save yang ada di ikanmodel.php untuk menyimpan data ke database
                         $data['path_gambar']=$upload['file']['file_name'];
			$this->ikanmodel->setDataPostIkan($data);
			
			redirect('ikancontroller/redirectPost'); // Redirect kembali ke halaman awal / halaman view data
		}else{ // Jika proses upload gagal
			$data['message'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
		}
		//gambar

	}
        function upload($nama) {
        $config['upload_path'] = './res/img/product/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = '2048';
        $config['remove_space'] = TRUE;
        $config['file_name'] = $nama;
        $this->upload->initialize($config);
        $this->load->library('upload', $config); // Load konfigurasi uploadnya
        if ($this->upload->do_upload('input_gambar')) { // Lakukan upload dan Cek jika proses upload berhasil
            // Jika berhasil :
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
            return $return;
        } else {
            // Jika gagal :
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
            echo $this->upload->display_errors();
            return $return;
        }
    }
        
}