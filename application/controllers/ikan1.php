<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends CI_Controller {

    public function index() {
        
    }

    function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation'));
        $this->load->helper(array('url', 'form'));
        $this->load->model('usermodel');
    }

    public function viewRegister() {
        $pesan = $this->session->flashdata('pesan');
        $pesan2 = $this->session->flashdata('warning');
        $status['message'] = $pesan;
        $status['warning'] = $pesan2;
        $this->load->view('register', $status);
    }

    public function register() {


        $this->form_validation->set_rules('email', 'Email', 'is_unique[user.email]'
        );

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('warning', 'emailsama');
            redirect('usercontroller/viewRegister');
        } else {


            $data['nama'] = $this->input->post('nama');
            $data['email'] = $this->input->post('email');
            $data['password'] = md5($this->input->post('password'));
            $data['alamat'] = $this->input->post('alamat');
            $data['no_hp'] = $this->input->post('nohp');

            $this->usermodel->setDataUser($data);

            $this->session->set_flashdata('pesan', 'berhasil');
            redirect('usercontroller/viewRegister');
        }
    }

    public function viewLogin() {
        $this->load->view('login');
    }

    public function login() {
        $username = $this->input->post('email');
        $password = $this->input->post('password');
        $status = $this->usermodel->getAuth($username, $password);
        if ($status == 'sukses') {
            if ($this->session->userdata('role') == 'admin') {
                redirect("usercontroller/daftaruser");
            }
            redirect(base_url());
        } else {
            $this->load->view('login');
        }
    }

    public function logout() {
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('id_login');
        $this->session->unset_userdata('id');
        $this->session->set_flashdata('logout', 'Anda berhasil logout');
        $this->session->unset_userdata('login');

        redirect('usercontroller/viewLogin');
    }

    public function viewProfil() {
        $id_user = $this->session->userdata('id_user');
        $data['users'] = $this->usermodel->getDataUser($id_user)->result();
        $this->load->view('profil', $data);
    }

    public function daftarUser() {
        $data["dUser"] = $this->usermodel->getDataAllUser();
        $this->load->view('ViewDaftarUser', $data);
    }

    public function redirectPost() { 
            $this->load->view('viewpostingIkan');
        
    }
}
