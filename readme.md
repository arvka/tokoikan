﻿
# TOKOIKAN

Tokoikan is a Indonesian fishery marketplace where site owner can sell fresh fish to the buyer. We made this web app to complete final project of Software Engineering course. This site has been made using code igniter framework 3.1.7. 

## GETTING STARTED

If you are not using git you can download the zip file, but if you use git start clone by using this command.

    git clone https://gitlab.com/arvka/tokoikan.git

Then deploy it using your existing web server app (e.g. : apache) and database server (this app using mysql with phpmyadmin for database management) 
### Prerequisites

 - Web server (apache, nginx, etc.)
 - MySQL
 - You can get all you need above by installing XAMPP 
 - PC
 - Stable electric supply
 - Good mood :)
 - Make sure you are still breathing


### Installing

Just install XAMPP, no need for further details. Download the file then extract or clone it to your web server folder and then import sql file (included in this project) to mysql server. 
If you feel lost, search it on Google for solution. If the problem still exist, face your fate. 


## Built With

* [Code Igniter](https://codeigniter.com/download) - The web framework used
* [XAMPP](https://www.apachefriends.org/download.html) - Web deployment starterpack
* [Visual Studio Code](https://code.visualstudio.com/) - Text editor
* Brain - Used for thinking
* Mouth - Used for swearing in javanese if errors occurr
* Hand - For typing and do useless thing for relaxation


## Our Team

* **Arka Fadila Yasa** - *Initial work, Frontend programmer, person who write this sh!t readme file* - [arvka](https://gitlab.com/arvka)
* **Arlian Gutama** - *Project Manager* - [gutamaarlian](https://gitlab.com/gutamaarlian)
* **Mohammad Ilham** - *Backend Programmer* - [deadser](https://gitlab.com/deadser)
* **Fatimah Azzahra** - *Documentation, The only women in our team*
* **Ahmad Ichsan Fauzi** 


## License

See in license page and of course each member of this team have driving license.

## Thanks to 

 - Allah, our almighty god. So we can complete this project.
 - Our parents.
 - Sofware engineering lecture.
 - My lov... eh wait, i'm still single.
 - Random person who read this readme file, congratulations !! yout are the one of the rarest person in the Round Earth.
 

> *VIOLETS ARE RED, ROSES ARE BLUE. I HAVE TO GO TO THE BATHROOM.*
>  -Patrick Star

